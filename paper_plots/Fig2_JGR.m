% Now let's make a nice little plot for the simulation where q_s
% is imposed at the upstream node and a wave of sediment traverses the
% domain.
%
%
% For first sed fig load up_q_b_WDG30_dt1800s.mat
% And for the updated grid: 
% load runs/up_q_b_WDG30_dt900s_dx100.mat
% then run 
% what_you_need_in_ode_aftermath.m

x_stag = linspace(cst.dx./2,cst.dx.*(cst.M-1)-cst.dx./2,cst.M-1).*1e-3;
x_plot = linspace(0,cst.dx.*(cst.M-1),cst.M).*1e-3;

f1 = figure;
f1.Units = 'centimeters';
f1.Position(3) = 9;
f1.Position(4) = 8;
sy = 3;
sx = 1;


dt_fact = 3600 ./ cst.dt; %Multiplier needed to get time in hours
if sim_cond.upstream_qs_flux == 1;
    %OLD %pos_qt = [1 3 6 8 10 12 18]; %For dt = 1800s. That way things can be seen on the plot
    %pos_qt = [1 2 3 4 6 7 8]; %For dt = 1800s. That way things can be seen on the plot
    pos_qt = [1 2 3 4 6 7 8].*2; %For dt = 900s.
    %pos_qt = [2 3 4 6 7 10 12]; %For dt = 1200s.

end
%pos_qt = [1 100 150 200 250 300 800].*dt_fact;
if sim_cond.distributed_Vs_input == 1;
    % That was for 50km parabolic glacier
    %pos_qt = [1 50 75 100 125 150 400]; % For dt = 1800s and initial sed layer of 0.5m
    %pos_qt = [1 100 150 200 250 300 800]; % For dt = 1800s and initial sed layer of 1m
    % That is now for WDG glacier 30 km long
    pos_qt = [1 10 20 30 40 200 400]; % For dt = 1800s and initial sed layer of 0.5m
    %pos_qt = [1 10 20 30 40 200 380]; % For dt = 1800s and initial sed layer of 1m

end

for i=1:length(pos_qt)
    leg_hr_str{i} = num2str(pos_qt(i)./dt_fact);
end

sb_p = 1;
S2 = subplot(sy,sx,sb_p);
    % Now let's plot the sediment volume
    D_i_norm = 0;
    if D_i_norm == 1
        Va_D_corr = store_var.V_a ./ D_i;
    else
        Va_D_corr = store_var.V_a;
    end
    p2a = plot(x_plot,Va_D_corr(pos_qt(1),:));
    hold on
    p2b = plot(x_plot,Va_D_corr(pos_qt(2),:));
    p2c = plot(x_plot,Va_D_corr(pos_qt(3),:));
    p2d = plot(x_plot,Va_D_corr(pos_qt(4),:));
    p2e = plot(x_plot,Va_D_corr(pos_qt(5),:));
    p2f = plot(x_plot,Va_D_corr(pos_qt(6),:));
    p2g = plot(x_plot,Va_D_corr(pos_qt(7),:));

    l2 = legend(strcat('V_{s}, t=',leg_hr_str{1},'hrs'),strcat('V_{s}, t=',leg_hr_str{2},'hrs'),...
        strcat('V_{s}, t=',leg_hr_str{3},'hrs'),strcat('V_{s}, t=',leg_hr_str{4},'hrs'),strcat('V_{s}, t=',leg_hr_str{5},'hrs'),...
        strcat('V_{s}, t=',leg_hr_str{6},'hrs'),strcat('V_{s}, t=',leg_hr_str{7},'hrs'));
    l2.Location = 'eastoutside';
    l2.Visible = 'off';

    grid on


sb_p = sb_p +1;
S1 = subplot(sy,sx,sb_p);
    % start with transport capacity
    p1a = plot(x_plot,q_tc(2,:),'k--');
    hold on

    p1_blank = plot(2,0.025,'x');
    p1_blank.Color = 'none';
    
    % now plot actual transport at the following positions
    p1b = plot(x_plot,q_b(pos_qt(1),:));
    p1b.Color = p2a.Color;
    p1c = plot(x_plot,q_b(pos_qt(2),:));
    p1c.Color = p2b.Color;
    p1d = plot(x_plot,q_b(pos_qt(3),:));
    p1d.Color = p2c.Color;
    p1e = plot(x_plot,q_b(pos_qt(4),:));
    p1e.Color = p2d.Color;
    p1f = plot(x_plot,q_b(pos_qt(5),:));
    p1f.Color = p2e.Color;
    p1g = plot(x_plot,q_b(pos_qt(6),:));
    p1g.Color = p2f.Color;
    p1h = plot(x_plot,q_b(pos_qt(7),:));
    p1h.Color = p2g.Color;

    [l1,icons,plots,legend_text] = legend('q_{tc}','q_{t} @ (hrs)',strcat('t=',leg_hr_str{1}),...
        strcat('t=',leg_hr_str{2}),strcat('t=',leg_hr_str{3}),strcat('t=',leg_hr_str{4}),...
        strcat('t=',leg_hr_str{5}),strcat('t=',leg_hr_str{6}),strcat('t=',leg_hr_str{7}));
    l1.Location = 'eastoutside';
    l1.Box = 'off';

    l1_tick_l = 0.3;
    for i_leg = length(legend_text)+1:2:length(icons)
        icons(i_leg).XData(1) = icons(i_leg).XData(2) - l1_tick_l;
    end

    grid on

sb_p = sb_p +1;
S3 = subplot(sy,sx,sb_p);
    % Finally, let's plot the fraction covered
    p3a = plot(x_plot,V_ratio(pos_qt(1),:));
    hold on
    p3b = plot(x_plot,V_ratio(pos_qt(2),:));
    p3c = plot(x_plot,V_ratio(pos_qt(3),:));
    p3d = plot(x_plot,V_ratio(pos_qt(4),:));
    p3e = plot(x_plot,V_ratio(pos_qt(5),:));
    p3f = plot(x_plot,V_ratio(pos_qt(6),:));
    p3g = plot(x_plot,V_ratio(pos_qt(7),:));


    l3 = legend(strcat('F_{c}, t=',leg_hr_str{1},'hrs'),strcat('F_{c}, t=',leg_hr_str{2},'hrs'),...
        strcat('F_{c}, t=',leg_hr_str{3},'hrs'),strcat('F_{c}, t=',leg_hr_str{4},'hrs'),strcat('F_{c}, t=',leg_hr_str{5},'hrs'),...
        strcat('F_{c}, t=',leg_hr_str{6},'hrs'),strcat('F_{c}, t=',leg_hr_str{7},'hrs'));
    l3.Location = 'eastoutside';
    l3.Visible = 'off';
    
    grid on

%%%%%%%%%%%%%%%
% And making it tidy again
%LW = 2;
LW = 1.5;
FS = 8;
%FS = 28;

p1a.LineWidth = LW;
p1b.LineWidth = LW;
p1c.LineWidth = LW;
p1d.LineWidth = LW;
p1e.LineWidth = LW;
p1f.LineWidth = LW;
p1g.LineWidth = LW;
p1h.LineWidth = LW;

p2a.LineWidth = LW;
p2b.LineWidth = LW;
p2c.LineWidth = LW;
p2d.LineWidth = LW;
p2e.LineWidth = LW;
p2f.LineWidth = LW;
p2g.LineWidth = LW;

p3a.LineWidth = LW;
p3b.LineWidth = LW;
p3c.LineWidth = LW;
p3d.LineWidth = LW;
p3e.LineWidth = LW;
p3f.LineWidth = LW;
p3g.LineWidth = LW;

S1.FontSize = FS;
S2.FontSize = FS;
S3.FontSize = FS;

S1.YLabel.String = 'q_{tc}; q_{t} (m^2 s^{-1})';
if D_i_norm == 1
    S2.YLabel.String = 'V_{s} (m^3/m^2)';
else
    S2.YLabel.String = 'V_{s} (m^3/m)';
end
S3.YLabel.String = 'r_{V}';

S3.XLabel.String = 'Distance from water source (km)';

S1.XMinorTick = 'on';
S1.YMinorTick = 'on';
S2.XMinorTick = 'on';
S2.YMinorTick = 'on';
S3.XMinorTick = 'on';
S3.YMinorTick = 'on';

%Making plots look the same
l1.Position(1) = 0.725;
l1.Position(2) = 0.357;
icons(2).Position(1) = 0.18;
sub_w = 0.60;
S1.Position(3) = sub_w;
S2.Position(3) = sub_w;
S3.Position(3) = sub_w;

sub_h = 0.23;
S1.Position(4) = sub_h;
S2.Position(4) = sub_h;
S3.Position(4) = sub_h;

sub_x = 0.15;
S1.Position(1) = sub_x;
S2.Position(1) = sub_x;
S3.Position(1) = sub_x;

S1.Position(2) = S1.Position(2) + 0.02;
S2.Position(2) = S2.Position(2) + 0.025;
S3.Position(2) = S3.Position(2) + 0.03;


if sim_cond.upstream_qs_flux == 1;
    S1.YLim = [0 0.12];
    S2.YLim = [0 0.1];
end
if sim_cond.distributed_Vs_input == 1;
    S1.YLim = [0 0.12];
    %S2.YLim = [0 2.8];
    %S2.YLim = [0 6]; FOR parabolic glacier
    %S2.YLim = [0 8.3]; % for WDG glacier
    S2.YLim = [0 15]; % for WDG glacier and eta 1m
end
%Adding text on there
subplot(S1)
%xposfact = 0.05;
xposfact = 0.95;
xposfact2 = 0.96;
yposfact = 0.85;
%yposfact = 0.15;
tA = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact,S1.YLim(2).*yposfact,'B');
subplot(S2)
tB = text(S2.XLim(2) - (S2.XLim(2)-S2.XLim(1)).*xposfact,S2.YLim(2).*yposfact,'A');
subplot(S3)
tC = text(S3.XLim(2) - (S3.XLim(2)-S3.XLim(1)).*xposfact,S3.YLim(2).*yposfact,'C');

tA.FontSize = FS;
tB.FontSize = FS;
tC.FontSize = FS;
