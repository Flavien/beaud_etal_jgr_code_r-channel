% This plot file is to compare the different shapes of eskers that accumulate as a function
% of glacier geometry.
%
%
% First you should run
% ST_ar = load_comp_runs();

%To recompute the sediment thickness
run_nb = length(ST_ar);
%Because last run too long
run_nb = run_nb -1;

eta_s = zeros(ST_ar(1).cst.t_end./ST_ar(1).cst.dt,ST_ar(1).cst.M,run_nb);
eta_plot = zeros(ST_ar(1).cst.t_end./ST_ar(1).cst.dt,ST_ar(1).cst.M);

eta_s_Fa = zeros(ST_ar(run_nb+1).cst.t_end./ST_ar(run_nb+1).cst.dt,ST_ar(run_nb+1).cst.M);

for i = 1:run_nb
    eta_s(:,:,i) = (ST_ar(i).V_a ./ ST_ar(i).D_i) .* (1./(1-ST_ar(i).cst.lambda_s));
end
eta_s_Fa = (ST_ar(run_nb+1).V_a ./ ST_ar(run_nb+1).D_i) .* (1./(1-ST_ar(run_nb+1).cst.lambda_s));


dx = ST_ar(1).cst.dx;
M = ST_ar(1).cst.M;

x_stag = linspace(dx./2,dx.*(M-1)-dx./2,M-1).*1e-3;
x_plot = linspace(0,dx.*(M-1),M).*1e-3;


seas_start = 185*24;
seas_end = (185+115)*24;
%
sed_start = (185+85)*24;
sed_finish = seas_end;


% t_span of plot in days
t_span_d = ([sed_start:sed_finish]-seas_start).*ST_ar(1).cst.dt ./ 3600 ./ 24 ;
t_span_900 = ([sed_start:sed_finish]-seas_start).*ST_ar(run_nb+1).cst.dt ./ 3600 ./ 24 .*4 ;

% And to plot that dt = 900 sim
%im_test = imagesc(x_plot(term_slice),t_span_900,eta_s_Fa(sed_start*4:sed_finish*4,term_slice));

% window along the profile
%term_slice = [92:101];
%For dx = 100m
term_slice = [285:301];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FS = 10;
FSax = 8;
LW = 2;
LWms = 1;
MS = 10;
sx = 4;
sy = 2;


cbar_limits = [0 1.2];

% NOTE ABOUT FIGURE WIDTH FOR AGU
% figure width = 20pc. 1pc = 0.42cm
% 20pc = 8.4 cm 2 col fig width whould be around 16.8cm.
f1 = figure;
f1.Units = 'centimeters';
%f1.Position(3) = 16.8;
f1.Position(3) = 13;
f1.Position(4) = 14;


spos = 1;
S1 = subplot(sx,sy,spos);
    k = 1;
    im1 = imagesc(x_plot(term_slice),t_span_d,eta_s(sed_start:sed_finish,term_slice,k));
    c1 = colorbar;
    caxis([cbar_limits(1) cbar_limits(2)]);
    %t1 = title('WDG');
    txt1 = text(28.6,110,'700 m wedge');

spos = spos + 1;
S2 = subplot(sx,sy,spos);
    k = k +1;
    im2 = imagesc(x_plot(term_slice),t_span_d,eta_s(sed_start:sed_finish,term_slice,k));
    c2 = colorbar;
    caxis([cbar_limits(1) cbar_limits(2)]);
    %t2 = title('CUBIC');
    txt2 = text(28.6,110,'700 m power law');
    
spos = spos + 1;
S3 = subplot(sx,sy,spos);
    k = k +1;
    im3 = imagesc(x_plot(term_slice),t_span_d,eta_s(sed_start:sed_finish,term_slice,k));
    c3 = colorbar;
    caxis([cbar_limits(1) cbar_limits(2)]);
    %t3 = title('800');
    txt3 = text(28.6,110,'800 m wedge');
    
spos = spos + 1;
S4 = subplot(sx,sy,spos);
    k = k +1;
    im4 = imagesc(x_plot(term_slice),t_span_d,eta_s(sed_start:sed_finish,term_slice,k));
    c4 = colorbar;
    caxis([cbar_limits(1) cbar_limits(2)]);
    %t4 = title('600');
    txt4 = text(28.6,110,'600 m wedge');

spos = spos + 1;
S5 = subplot(sx,sy,spos);
    k = k +1;
    im5 = imagesc(x_plot(term_slice),t_span_d,eta_s(sed_start:sed_finish,term_slice,k));
    c5 = colorbar;
    caxis([cbar_limits(1) cbar_limits(2)]);
    txt5 = text(28.6,110,'Q_{ch} x 0.8');
    
spos = spos + 1;
S6 = subplot(sx,sy,spos);
    k = k +1;
    im6 = imagesc(x_plot(term_slice),t_span_d,eta_s(sed_start:sed_finish,term_slice,k));
    c6 = colorbar;
    caxis([cbar_limits(1) cbar_limits(2)]);
    %t6 = title('600');
    txt6 = text(28.6,110,'Q_{ch} x 1.2');
    
spos = spos + 1;
S7 = subplot(sx,sy,spos);
    k = k +1;
    im7 = imagesc(x_plot(term_slice),t_span_d,eta_s(sed_start:sed_finish,term_slice,k));
    c7 = colorbar;
    caxis([cbar_limits(1) cbar_limits(2)]);
    %t7 = title('600');
    txt7 = text(28.6,110,'q_{ls}(x=0) = 0.025');
    
spos = spos + 1;
S8 = subplot(sx,sy,spos);
    k = k +1;
    %im8 = imagesc(x_plot(term_slice),t_span_d,eta_s(sed_start:sed_finish,term_slice,k));
    im8 = imagesc(x_plot(term_slice),t_span_900,eta_s_Fa(sed_start*4:sed_finish*4,term_slice));
    c8 = colorbar;
    caxis([cbar_limits(1) cbar_limits(2)]);
    %t8 = title('600');
    txt8 = text(28.6,110,'q_{ls}(x=0) = 0.035');




%%%%%%%%%%%%%%%%%%%%%
%% MAKING EVERYTHING LOOK PRETTY

S1.FontSize = FSax;
S2.FontSize = FSax;
S3.FontSize = FSax;
S4.FontSize = FSax;
S5.FontSize = FSax;
S6.FontSize = FSax;
S7.FontSize = FSax;
S8.FontSize = FSax;

S1.YLabel.FontSize = FSax;
S7.XLabel.FontSize = FSax;
S8.XLabel.FontSize = FSax;

txt1.FontSize = FSax+1;
txt2.FontSize = FSax+1;
txt3.FontSize = FSax+1;
txt4.FontSize = FSax+1;
txt5.FontSize = FSax+1;
txt6.FontSize = FSax+1;
txt7.FontSize = FSax+1;
txt8.FontSize = FSax+1;

c1.Limits = cbar_limits;
c2.Limits = cbar_limits;
c3.Limits = cbar_limits;
c4.Limits = cbar_limits;
c5.Limits = cbar_limits;
c6.Limits = cbar_limits;
c7.Limits = cbar_limits;
c8.Limits = cbar_limits;

cmap = colormap('bone');
cmap = flip(cmap);
colormap(cmap)

S1.XTickLabel = '';
S2.XTickLabel = '';
S3.XTickLabel = '';
S4.XTickLabel = '';
S5.XTickLabel = '';
S6.XTickLabel = '';

S2.YTickLabel = '';
S4.YTickLabel = '';
S6.YTickLabel = '';
S8.YTickLabel = '';

S1.YLabel.String = 'Time (day)';

S8.XLabel.String = 'Distance from water source (km)';

c1.Visible = 'off';
c2.Visible = 'off';
c3.Visible = 'off';
c5.Visible = 'off';
c6.Visible = 'off';
c7.Visible = 'off';
c8.Visible = 'off';

sub_w = 0.35;
S1.Position(3) = sub_w;
S2.Position(3) = sub_w;
S3.Position(3) = sub_w;
S4.Position(3) = sub_w;
S5.Position(3) = sub_w;
S6.Position(3) = sub_w;
S7.Position(3) = sub_w;
S8.Position(3) = sub_w;

sub_h = 0.21;
S1.Position(4) = sub_h;
S2.Position(4) = sub_h;
S3.Position(4) = sub_h;
S4.Position(4) = sub_h;
S5.Position(4) = sub_h;
S6.Position(4) = sub_h;
S7.Position(4) = sub_h;
S8.Position(4) = sub_h;

%sub_x1 = 0.12;
%S1.Position(1) = sub_x1;
%S3.Position(1) = sub_x1;

sub_x2 = 0.50;
S2.Position(1) = sub_x2;
S4.Position(1) = sub_x2;
S6.Position(1) = sub_x2;
S8.Position(1) = sub_x2;

%sub_y1 = 0.58;
%S1.Position(2) = sub_y1;
%S2.Position(2) = sub_y1;

%sub_y2 = 0.18;
%S3.Position(2) = sub_y2;
%S4.Position(2) = sub_y2;

c4.Position = [0.863 0.2205 0.0412 0.6703];
c4.Label.String =  'Sediment Thickness (m)';
colormap(cmap)

subplot(S8)
S1.YLabel.Position = [27.9786 147.1312 1];
S8.XLabel.Position = [28.3143 122.0136 1];

%Adding text on there
subplot(S1)
%xposfact = 0.05;
xposfact = 0.95;
%xposfact2 = 0.96;
yposfact2 = 0.85;
tA = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact,S1.YLim(2) - (S1.YLim(2)-S1.YLim(1)).*yposfact2,'A');
subplot(S2)
tB = text(S2.XLim(2) - (S2.XLim(2)-S2.XLim(1)).*xposfact,S2.YLim(2) - (S2.YLim(2)-S2.YLim(1)).*yposfact2,'B');
subplot(S3)
tC = text(S3.XLim(2) - (S3.XLim(2)-S3.XLim(1)).*xposfact,S3.YLim(2) - (S3.YLim(2)-S3.YLim(1)).*yposfact2,'C');
subplot(S4)
tD = text(S4.XLim(2) - (S4.XLim(2)-S4.XLim(1)).*xposfact,S4.YLim(2) - (S4.YLim(2)-S4.YLim(1)).*yposfact2,'D');
subplot(S5)
tE = text(S5.XLim(2) - (S5.XLim(2)-S5.XLim(1)).*xposfact,S5.YLim(2) - (S5.YLim(2)-S5.YLim(1)).*yposfact2,'E');
subplot(S6)
tF = text(S6.XLim(2) - (S6.XLim(2)-S6.XLim(1)).*xposfact,S6.YLim(2) - (S6.YLim(2)-S6.YLim(1)).*yposfact2,'F');
subplot(S7)
tG = text(S7.XLim(2) - (S7.XLim(2)-S7.XLim(1)).*xposfact,S7.YLim(2) - (S7.YLim(2)-S7.YLim(1)).*yposfact2,'G');
subplot(S8)
tH = text(S8.XLim(2) - (S8.XLim(2)-S8.XLim(1)).*xposfact,S8.YLim(2) - (S8.YLim(2)-S8.YLim(1)).*yposfact2,'H');

tA.FontSize = FS;
tB.FontSize = FS;
tC.FontSize = FS;
tD.FontSize = FS;
tE.FontSize = FS;
tF.FontSize = FS;
tG.FontSize = FS;
tH.FontSize = FS;

