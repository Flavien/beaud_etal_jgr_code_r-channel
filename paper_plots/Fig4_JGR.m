% This file aims at creating a plot or plots possibly that will be helpful to compare different simulations
% I think in here I will only use temporal evolution and plot the qties at different locations
%
%
%Before running the plotting script, you should create the array of simulation structures.
% in vi ../load_and_prep/load_comp_runs.m
% load_run = list_freq;
% Then run: "ST_ar = load_comp_runs;"


dx = ST_ar(1).cst.dx;
M = ST_ar(1).cst.M;

x_stag = linspace(dx./2,dx.*(M-1)-dx./2,M-1).*1e-3;
x_plot = linspace(0,dx.*(M-1),M).*1e-3;

t_plot = [1:1:ST_ar(1).cst.tstp_nb]./3600.*ST_ar(1).cst.dt ./ 24;

sim_nb = length(ST_ar);
% Won't work for now because sims have different lengths...
%for j = 1:xloc_nb
%    q_ttc(j,:,:) = ST_ar(j).q_tc .* ST_ar(j).D_i;
%    q_tt(j,:,:) = ST_ar(j).q_b .* ST_ar(j).D_i;
%end

% SETTING VALUES FOR WHERE TO START

ii = 3; 
ij = ii + 1;
jj = ii+1;
comp_runs = 1;

j = 1;
q_ttc1 = ST_ar(j).q_tc .* ST_ar(j).D_i;
q_tt1 = ST_ar(j).q_b .* ST_ar(j).D_i;
%jj = j+1;
q_ttc2 = ST_ar(jj).q_tc .* ST_ar(jj).D_i;
q_tt2 = ST_ar(jj).q_b .* ST_ar(jj).D_i;

f1 = figure;
f1.Units = 'centimeters';
f1.Position(3) = 15;
f1.Position(4) = 21;

sx = 1;
sy = 5;

LW = 2.5;
LW1 = 2;
LW2 = 1.5;
FSax = 8;
FStxt = 10;
MS = 8;

Tmonth = 24*30;
if ii == 1 || ii == 2
    % For 1d
    Tstep = 1;
    Tcrop = [Tmonth*3:Tstep:Tmonth*3 + 3*24];
elseif ii == 3 || ii == 4
    % For 6d
    Tstep = 3;
    Tcrop = [Tmonth*3:Tstep:Tmonth*3 + 6*24*3];
elseif ii == 5 || ii == 6;
    % For 1m
    Tstep = 6;
    Tcrop = [Tmonth*3:Tstep:Tmonth*3*2];
elseif ii == 7 || ii == 8;
    % For 3m
    Tstep = 12;
    Tcrop = [Tmonth*3:Tstep:Tmonth*3*3];
end

%Xloc = [M-5 M-3 M-1 M];
Xloc = [M-5 M];
Xloc_st = Xloc - 1;

M_list = {'s','v','<','>','d','x'};
%col_list = [0 0.4470 0.7410; 0.8500 0.3250 0.0980; 0.9290 0.6940 0.1250];
%col_list = [0.4940 0.1840 0.5560; 0.9290 0.6940 0.1250; 0.8500 0.3250 0.0980; 0 0.4470 0.7410];
col_list = [0.9290 0.6940 0.1250; 0 0.4470 0.7410];
xloc_nb = length(Xloc);

%%%% To use instead of plotyy
% yyaxis left
% yyaxis right
%i = 1;

s_k = 1;
S1 = subplot(sy,sx,s_k);
    si = 1;
    S1 = subplot(sy,sx,si);
        
        for jj = 1:xloc_nb
            %p12(jj) = plot(t_plot(Tcrop),ST_ar(ii).V_a(Tcrop,Xloc(jj))./ST_ar(ii).D_i(Tcrop,Xloc(jj)),'Linewidth',LW1);
            p12(jj) = plot(t_plot(Tcrop),ST_ar(ii).V_a(Tcrop,Xloc(jj)),'Linewidth',LW1);
            hold on
            %p12(jj).LineStyle = '--';
            p12(jj).Color = col_list(jj,:);
            
            a = '@ ';
            b = num2str((Xloc(jj)-1).*ST_ar(ii).cst.dx ./ 1e3);
            c = 'km';
            %Leg_txtA{jj} = strcat({a},{b},{c});
            Leg_txtA{jj} = [a b c];

        end
        %S1.YLabel.String = 'V_s / D_i (m)';
        S1.YLabel.String = 'V_s (m^2)';
        if comp_runs == 1
            %yyaxis right
            for jj=1:xloc_nb
                %p11(jj) = plot(t_plot(Tcrop),ST_ar(ij).V_a(Tcrop,Xloc(jj))./ST_ar(ij).D_i(Tcrop,Xloc(jj)),'Linewidth',LW2);
                p11(jj) = plot(t_plot(Tcrop),ST_ar(ij).V_a(Tcrop,Xloc(jj)),'Linewidth',LW2);
                hold on
                p11(jj).LineStyle = '--';
                p11(jj).Color = col_list(jj,:);

                
                a2 = '@ ';
                b2 = num2str((Xloc(jj)-1).*ST_ar(ii).cst.dx ./ 1e3);
                c2 = 'km; decoup.';
                %Leg_txtA{jj} = strcat({a},{b},{c});
                Leg_txtB{jj} = [a2 b2 c2];
            end
            %S1.YAxis(2).Color = [1 1 1].*0.4; % To just tackle the right axis
            %S1.YLabel.String = 'Uncoupled V_s / D_i (m); dashed';
            %S1.YLabel.String = 'Uncoupled V_s (m^2); dashed';
        end

        % KEEP THAT HERE if you want the discharge plotted on the first plot.
        yyaxis right
        p23(1) = plot(t_plot(Tcrop),ST_ar(ii).Qch(Tcrop,Xloc(end)),'Linewidth',LW);
        p23(1).Color = [1 1 1].*0.75;
        S1.YAxis(2).Color = [1 1 1].*0.4; % To just tackle the right axis
        S1.YLim = [45 105];
        S1.YLabel.String = 'Q_{ch}(x=X_L) (m^3/s)';

        %Leg_txtA{jj} = strcat({'@ '},num2str((Xloc(jj)).*ST_ar(ii).cst.dx ./ 1e3),'km');
        %Leg_txtA = {'@ km','@ 29.7 km','@ 30 km','@ 29.1 km uncoupled','@ 29.7 km uncoupled','@ 30 km uncoupled'};
        if comp_runs == 0
            legA = legend(p12,Leg_txtA);
            legA.Orientation = 'horizontal';
            legA.Position(1) = 0.18;
            legA.Position(2) = 0.93;
        elseif comp_runs ==1 
            legA = legend([p12 p11],[Leg_txtA Leg_txtB]);
            legA.Orientation = 'horizontal';
            %legA.Position(1) = 0.13;
            %legA.Position(2) = 0.9334;
        end

    si = si+1;
    S2 = subplot(sy,sx,si);
        for jj = 1:xloc_nb

            if comp_runs == 1
                p21(jj) = plot(t_plot(Tcrop),ST_ar(ij).Sch(Tcrop,Xloc(jj)),'Linewidth',LW2);
                hold on
                p21(jj).LineStyle = '--';
                p21(jj).Color = col_list(jj,:);
            end
            p22(jj) = plot(t_plot(Tcrop),ST_ar(ii).Sch(Tcrop,Xloc(jj)),'Linewidth',LW1);
            p22(jj).Color = col_list(jj,:);
            hold on
        end
        S2.YLabel.String = 'S_{ch} (m^2)';

        %yyaxis right
        %p23(1) = plot(t_plot(Tcrop),ST_ar(ii).Qch(Tcrop,Xloc(end)),'Linewidth',LW2);
        %p23(1).Color = [1 1 1].*0.65;
        %S2.YAxis(2).Color = [1 1 1].*0.4; % To just tackle the right axis
        %S2.YLim = [45 105];
        %S2.YLabel.String = 'Q_{ch} (m^3/2)';


    si = si+1;
    S3 = subplot(sy,sx,si);
    
        for jj = 1:xloc_nb
            if comp_runs == 1
                p31(jj) = plot(t_plot(Tcrop),abs(ST_ar(ij).dphicdx(Tcrop,Xloc_st(jj))),'Linewidth',LW2);
                hold on
                p31(jj).LineStyle = '--';
                p31(jj).Color = col_list(jj,:);
            end
            p32(jj) = plot(t_plot(Tcrop),abs(ST_ar(ii).dphicdx(Tcrop,Xloc_st(jj))),'Linewidth',LW1);
            p32(jj).Color = col_list(jj,:);
            hold on
        end
        
        S3.YLabel.String = '|d \phi_{ch} /dx| (Pa/m)';

    si = si+1;
    S4 = subplot(sy,sx,si);

        for jj = 1:xloc_nb
            if comp_runs == 1
                p41(jj) = plot(t_plot(Tcrop),ST_ar(ij).q_ttc(Tcrop,Xloc(jj)),'Linewidth',LW2);
                hold on
                p41(jj).LineStyle = '--';
                p41(jj).Color = col_list(jj,:);
            end
            p42(jj) = plot(t_plot(Tcrop),ST_ar(ii).q_ttc(Tcrop,Xloc(jj)),'Linewidth',LW1);
            p42(jj).Color = col_list(jj,:);
            hold on
        end

        S4.YLabel.String = 'q_{ttc} (m^3/s)';

    si = si+1;
    S5 = subplot(sy,sx,si);

        for jj = 1:xloc_nb
            if comp_runs == 1
                p51(jj) = plot(t_plot(Tcrop),ST_ar(ij).q_tt(Tcrop,Xloc(jj)),'Linewidth',LW2);
                %p51(jj) = semilogy(t_plot(Tcrop),ST_ar(ij).q_tt(Tcrop,Xloc(jj)),'Linewidth',LW2);
                hold on
                p51(jj).LineStyle = '--';
                p51(jj).Color = col_list(jj,:);
            end
            p52(jj) = plot(t_plot(Tcrop),ST_ar(ii).q_tt(Tcrop,Xloc(jj)),'Linewidth',LW1);
            %p52(jj) = semilogy(t_plot(Tcrop),ST_ar(ii).q_tt(Tcrop,Xloc(jj)),'Linewidth',LW1);
            p52(jj).Color = col_list(jj,:);
            hold on
        end

        S5.YLabel.String = 'q_{tt} (m^3/s)';
        S5.XLabel.String = 'Days during simulation';


%%%%%%%%%%
% Making shit neat again

S1.FontSize = FSax;
S2.FontSize = FSax;
S3.FontSize = FSax;
S4.FontSize = FSax;
S5.FontSize = FSax;

% XLims
Xax_lims = [t_plot(Tcrop(1)) t_plot(Tcrop(end))];
S1.XLim = Xax_lims;
S2.XLim = Xax_lims;
S3.XLim = Xax_lims;
S4.XLim = Xax_lims;
S5.XLim = Xax_lims;

% YLims for 6d D60 dx 100
S2.YLim = [33 41];
S3.YLim = [0 125];
S5.YLim = [0 0.18];

% Adding some minor ticks on subplot
subplot(S1)
S1.XMinorTick = 'on';
S1.XGrid = 'on';
yyaxis left
S1.YMinorTick = 'on';
S1.YGrid = 'on';
yyaxis right
S1.YMinorTick = 'on';

S2.XMinorTick = 'on';
S2.YMinorTick = 'on';
S2.XGrid = 'on';
S2.YGrid = 'on';

S3.XMinorTick = 'on';
S3.YMinorTick = 'on';
S3.XGrid = 'on';
S3.YGrid = 'on';

S4.XMinorTick = 'on';
S4.YMinorTick = 'on';
S4.XGrid = 'on';
S4.YGrid = 'on';

S5.XMinorTick = 'on';
S5.YMinorTick = 'on';
S5.XGrid = 'on';
S5.YGrid = 'on';

if comp_runs ==1 
    legA.Position(1) = 0.13;
    legA.Position(2) = 0.9334;
end

%legA.Position(1) = 0.32;
%legA.Position(2) = 0.812;
legA.Box = 'off';

%Adding text on there
subplot(S1)
%xposfact = 0.05;
xposfact = 0.97;
%xposfact2 = 0.96;
yposfact = 0.85;
yposfact2 = 0.15;
%tA = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact,S1.YLim(2).*yposfact,'A');
tA = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact,S1.YLim(2) - (S1.YLim(2)-S1.YLim(1)).*yposfact2,'A');
subplot(S2)
%tB = text(S2.XLim(2) - (S2.XLim(2)-S2.XLim(1)).*xposfact,S2.YLim(2).*yposfact,'B');
tB = text(S2.XLim(2) - (S2.XLim(2)-S2.XLim(1)).*xposfact,S2.YLim(2) - (S2.YLim(2)-S2.YLim(1)).*yposfact2,'B');
subplot(S3)
tC = text(S3.XLim(2) - (S3.XLim(2)-S3.XLim(1)).*xposfact,S3.YLim(2) - (S3.YLim(2)-S3.YLim(1)).*yposfact2,'C');
subplot(S4)
tD = text(S4.XLim(2) - (S4.XLim(2)-S4.XLim(1)).*xposfact,S4.YLim(2).*yposfact,'D');
subplot(S5)
tE = text(S5.XLim(2) - (S5.XLim(2)-S5.XLim(1)).*xposfact,S5.YLim(2).*yposfact,'E');

tA.FontSize = FStxt;
tB.FontSize = FStxt;
tC.FontSize = FStxt;
tD.FontSize = FStxt;
tE.FontSize = FStxt;
