% In this script, I use my old hysteresis script to 
% test what kind of hysteresis happens with the subglacial hydrology and sed transport
% I also plot discharge and V_ratio to show transport vs. supply limited conditions
%
% load runs_for_paper/Season/WDG_SEAS4_dx100_D60_qbF_dt900_BF.mat
% what_you_need_in_ode_aftermath
% 

dt_fact = 3600 ./ cst.dt;

% To make sure that the selection of the close-enough-to-terminus area is the same no matter what dx is
% Let's set that area as the last 20% of the profile, i.e. skip 80%
space_cut_off = round(0.8*cst.M) - 1;

for i=space_cut_off:1:cst.M
    if i == space_cut_off
        ii = 1;
    end
    hyst_season(ii,:) = hysteresis_test(Q_c,q_tt,i,dt_fact);
    %disp('NO DAILY fluctuations for first 70 days')
    fluctu_cut_off = 70;
    hyst_season(ii,1:fluctu_cut_off) = hyst_season(ii,1:fluctu_cut_off)*0 +0.5;
    ii = ii + 1;
end

x_plot = linspace(0,cst.dx.*(cst.M-1),cst.M).*1e-3;

 r_V_end = V_ratio(:,space_cut_off:end);
 alluvial = (r_V_end == 1);

%t_slice = [160*24+30*24:1:160*24*2];
%t_slice = [160*24+25*24:1:160*24*2-15*24];
t_slice = [160*24.*dt_fact+25*24.*dt_fact:1:160*24.*dt_fact*2-24*dt_fact*(20)];
t_slice2 = [t_slice(1)./(24.*dt_fact) : 1 : (t_slice(end)./(24.*dt_fact))-1] - 160;
t_plot = (t_slice-t_slice(1)) ./ (24.*dt_fact);

FSax = 8;
FS = 10;
MS = 8;
MS2 = 8;
MS3 = 2;
MS4 = 2;
MSL = 8;
MSL2 = 8;
MSL3 = 5;
LW1 = 2;
LW = 2;
LW2 = 1.5;

sx = 1;
sy = 2;

% What else will I need to plot?
% Q_c / V_ratio / mass conc and mass flux of sediment

f1 = figure;
f1.Units = 'centimeters';
f1.Position(1) = 2;
f1.Position(3) = 15;
f1.Position(4) = 9.0;

    si = 1;
    S1 = subplot(sy,sx,si);
        
        %p1a = plot(t_plot,out_mass_conc(t_slice));
        p1a = plot(t_plot,q_tt(t_slice,end-5));
        hold on
        p1b = plot(t_plot,q_tt(t_slice,end-2));
        p1c = plot(t_plot,q_tt(t_slice,end));

        col3 = p1a.Color;
        col1 = p1c.Color;
        p1a.Color = col1;
        p1c.Color = col3;
        

        %S1_Ymax = max(out_mass_conc(t_slice)).*1.1;
        S1_Ymax = max(q_tt(t_slice,end-2)).*1.4;
        S1.YLim = [0 S1_Ymax];
        %S1.YLabel.String = {'Sed. concentration';'(kg/m^3)'};
        S1.YLabel.String = {'Sediment discahrge';'(m^3/s)'};

        yyaxis right
        p1R = plot(t_plot,Q_c(t_slice));

        S1_Ymax2 = max(Q_c(t_slice)).*1.1;
        S1.YLim = [0 S1_Ymax2];
        S1.YLabel.String = 'Q_{ch}(x=X_L) (m^3/s)';

        p1R(1).Color = [1 1 1].*0.65;
        S1.YAxis(2).Color = [1 1 1].*0.4; % To just tackle the right axis
        
        l2 = legend('@ 29.5 km','@ 29.8 km','@ 30 km');
        l2.Location = 'North';;
        l2.Box = 'off';


    si = si + 1;
    S3 = subplot(sy,sx,si);
        % Keep what follows for one of the plots
        % NOTE Add space_cut_off-1 to hyst_span to get the actual node that you are
        % looking at.
        %hyst_span = [15 17 19 21];
        hyst_span = [57 60 62];
        % NOTE that hyst_span 1 is the furthest from the terminus
        for i=hyst_span
            if i == hyst_span(1)
                ii = 1;
            end
                %hyst_clock = find(hyst_season(i,:) == 1);
                %hyst_Cclock = find(hyst_season(i,:) == 0);
                %hyst_undef = find(hyst_season(i,:) == 0.5);
            hyst_clock = find(hyst_season(i,t_slice2) == 1);
            hyst_Cclock = find(hyst_season(i,t_slice2) == 0);
            hyst_undef = find(hyst_season(i,t_slice2) == 0.5);

            alluv_test = find(alluvial(t_slice,i) == 1);
            bedrock_test = find(alluvial(t_slice,i) ~= 1);

            p3a(ii) = plot(hyst_clock,hyst_clock.*0 + 0.6*ii,'o');

            hold on
            if isempty(hyst_Cclock) == 0
                p3b(ii) = plot(hyst_Cclock,hyst_Cclock.*0 + 0.6*ii,'o','MarkerFaceColor','k');
            end
            p3c(ii) = plot(hyst_undef,hyst_undef.*0 + 0.6*ii,'x');

            % Now plotting whether the channel is alluviated or not 
            p_al(ii) = plot(t_plot(alluv_test),alluvial(t_slice(alluv_test),i).*0+0.6*ii-0.15,'.');
            p_br(ii) = plot(t_plot(bedrock_test),alluvial(t_slice(bedrock_test),i).*0+0.6*ii-0.15,'.');

            ii = ii + 1;
        end
%%%%%%%%%%
%%% Now let's make that hysteresis pretty
%%%%%%%%
for ii = 1:length(p3a)
    p3a(ii).MarkerEdgeColor = 'k';
    p3c(ii).MarkerEdgeColor = [1 1 1].*0.4;
    p_al(ii).MarkerFaceColor = [1 1 1].*0.5;
    p_br(ii).MarkerFaceColor = [1 1 1].*1;
    p_al(ii).MarkerEdgeColor = [1 1 1].*0.5;
    p_br(ii).MarkerEdgeColor = [1 1 1].*1;
    p3a(ii).MarkerSize = MS2;
    p3c(ii).MarkerSize = MS;
    p_al(ii).MarkerSize = MS3;
    p_br(ii).MarkerSize = MS4;
    if ishandle(p3b(ii)) == 1 
        p3b(ii).MarkerSize = MS2;
        p3b(ii).MarkerEdgeColor = 'k';
    end
end

% And a bit prettier
ax1 = S3;
grid on
ax1.YTick = [0.6:0.6:0.6.*length(hyst_span)];
%ax1.YTickLabel = [40:1:50];
ax1.YTickLabel = (space_cut_off-1+hyst_span-1).*cst.dx.*1e-3;
ax1.FontSize = FSax;
%ax1.XLim = [25 120];
%ax1.XTick = [30:15:120];

yd = ylabel('Distance (km)');
xd = xlabel('Time during simulation (day)');

hyst_leg = 1;
if hyst_leg == 1
    %Making a stupid legend
    leg_ypos = 0.6.*length(hyst_span) + 0.3;
    pl1 = plot(57,leg_ypos,'x','Color',[1 1 1].*0.4);
    pl1.MarkerSize = MSL;
    tl1 = text(58,leg_ypos,'Undefined events');
    tl1.FontSize = FSax;

    pl2 = plot(72.5,leg_ypos,'ko');
    pl2.MarkerSize = MSL;
    tl2 = text(73.7,leg_ypos,'Clockwise');
    tl2.FontSize = FSax;


    pl3 = plot(83,leg_ypos,'ko');
    pl3.MarkerSize = MSL2;
    pl3.MarkerFaceColor = 'k';
    tl3 = text(84.3,leg_ypos,'Counter clockwise');
    tl3.FontSize = FSax;

    pl4 = plot(100,leg_ypos,'s');
    pl4.MarkerSize = MSL3;
    pl4.MarkerFaceColor = [1 1 1].*0.5;
    pl4.MarkerEdgeColor = [1 1 1].*0.5;
    tl4 = text(101,leg_ypos,'r_V = 1');
    tl4.FontSize = FSax;


    ax1.YLim = [0.4 2.3];
end
%%%%%%
%   pretty hysteresis is over..
%%%%%%%

%%%% Making the rest quite neat as well

% Line style
p1a.LineWidth = LW;
p1b.LineWidth = LW;
p1c.LineWidth = LW2;
p1R.LineWidth = LW1;

Xax_span = [49.5 t_plot(end)];
S1.XLim = Xax_span;
S3.XLim = Xax_span;

% Ticks
S1.XMinorTick = 'on';
S1.YMinorTick = 'on';
subplot(S1)
yyaxis left
S1.YMinorTick = 'on';

S3.XMinorTick = 'on';
% Legend
% FontSize
S1.FontSize = FSax;
S3.FontSize = FSax;

% Arranging subplots
S3.Position(4) = 0.38;
S1.Position(4) = 0.38;

%Adding text on there
subplot(S1)
%xposfact = 0.05;
xposfact = 0.97;
%xposfact2 = 0.96;
yposfact2 = 0.10;
tA = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact,S1.YLim(2) - (S1.YLim(2)-S1.YLim(1)).*yposfact2,'A');
subplot(S3)
tB = text(S3.XLim(2) - (S3.XLim(2)-S3.XLim(1)).*xposfact,S3.YLim(2) - (S3.YLim(2)-S3.YLim(1)).*yposfact2,'B');

tA.FontSize = FS;
tB.FontSize = FS;
