%Script to plot the results obtained with different timestep
%I will probably only show the changes in V_a and how it differs
%for the different tests and dt
%

% Let's start with the upstream flux scenario

us_flux_test    = 1;
init_05m        = 0;
init_1m         = 0;

three_lines = 1;

if us_flux_test == 1
    load runs/up_q_b_WDG30_dt450s.mat
    %load up_q_b_WDG30_dt900s_dx150.mat
    var_dt900 = store_var;

    load runs/up_q_b_WDG30_dt900s.mat
    %load up_q_b_WDG30_dt1200s_dx150.mat
    var_dt1800 = store_var;

    if three_lines == 1
        load runs/up_q_b_WDG30_dt3600s.mat
        var_dt1hr = store_var;
    end
elseif init_05m == 1
    load eta_05m_WDG30_dt900s.mat
    var_dt900 = store_var;

    load eta_05m_WDG30_dt1800s.mat
    var_dt1800 = store_var;

    load eta_05m_WDG30_dt3600s.mat
    var_dt1hr = store_var;
elseif init_1m == 1;
    load benchmark_SS50_q_b_eta_1_Wch_dt900s.mat
    var_dt900 = store_var;

    load benchmark_SS50_q_b_eta_1_Wch_dt1800s.mat
    var_dt1800 = store_var;
end




x_stag = linspace(cst.dx./2,cst.dx.*(cst.M-1)-cst.dx./2,cst.M-1).*1e-3;
x_plot = linspace(0,cst.dx.*(cst.M-1),cst.M).*1e-3;

LW1 = 2.5;
LW2 = 2;
LW3 = 2;
MS = 6;
FSax = 8;


col1 = rgb('MediumBlue');
col2 = rgb('Chartreuse');
col3 = rgb('Crimson');

f1 = figure;
f1.Units = 'centimeters';
f1.Position(3) = 15;
f1.Position(4) = 5;

    if us_flux_test == 1
        pos_qt = [1 2 3 4];
    elseif init_05m == 1
        pos_qt = [1 10 20 30 100];
    elseif init_1m == 1
        pos_qt = [1 50 100 200];
    end

    x_plot_mat = repmat(x_plot,length(pos_qt),1);

    p1 = plot(x_plot_mat',var_dt900.V_a(pos_qt.*8,:)','LineWidth',LW1,'Color',col1);
    hold on
    % For 1800s
    p2 = plot(x_plot_mat',var_dt1800.V_a(pos_qt.*4,:)','--','LineWidth',LW2,'Color',col2);
    % For 1200s
    %p2 = plot(x_plot_mat',var_dt1800.V_a(pos_qt.*3,:)','x','LineWidth',LW,'Color',[1 1 1].*0.35);
    if three_lines == 1
        p3 = plot(x_plot_mat',var_dt1hr.V_a(pos_qt,:)',':','LineWidth',LW3,'Color',col3);
    end

    ax = gca;

    ax.YLabel.String = 'V_{s} (m^3/m)';
    ax.XLabel.String = 'Distance from water source (km)';

    if three_lines == 1
        l1 = legend([p1(1) p2(1) p3(1)],'dt = 450s','dt = 900s','dt = 3600s');
    elseif three_lines == 0
        %l1 = legend([p1(1) p2(1)],'dt = 900s','dt = 1800s');
        l1 = legend([p1(1) p2(1)],'dt = 900s','dt = 1200s');
    end
    l1.Location = 'SouthWest';
    l1.Box = 'off';

    ax.FontSize = FSax;

    if init_05m == 1
        %ax.YLim = [0 3.8];
        ax.YLim = [0 8.2];
    elseif init_1m == 1
        ax.YLim = [0 7];
    end

