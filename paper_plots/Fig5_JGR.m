% This file builds on the temporal_sim_cond plotting file but aim at condensing all the simulations in a single
% plot
%
%
%Before running the plotting script, you should create the array of simulation structures.
% in vi ../load_and_prep/load_comp_runs.m
% load_run = list_freq;
% Then run: "ST_ar = load_comp_runs;"


dx = ST_ar(1).cst.dx;
M = ST_ar(1).cst.M;

x_stag = linspace(dx./2,dx.*(M-1)-dx./2,M-1).*1e-3;
x_plot = linspace(0,dx.*(M-1),M).*1e-3;

t_plot = [1:1:ST_ar(1).cst.tstp_nb]./3600.*ST_ar(1).cst.dt ./ 24;

sim_nb = length(ST_ar);
% Won't work for now because sims have different lengths...
%for j = 1:xloc_nb
%    q_ttc(j,:,:) = ST_ar(j).q_tc .* ST_ar(j).D_i;
%    q_tt(j,:,:) = ST_ar(j).q_b .* ST_ar(j).D_i;
%end

% SETTING VALUES FOR WHERE TO START

f1 = figure;
f1.Units = 'centimeters';
f1.Position(3) = 13;
f1.Position(4) = 17;

sx = 2;
sy = 4;

LW = 2.5;
LW1 = 2;
LW2 = 1.5;
FSax = 8;
FStxt = 10;
MS = 8;

Tmonth = 24*30;
%%%%%%%%%%%%%%%%%
%%% DEFINING A BUNCH OF TIME WINDOWS
% For 1d
Tstep = 1;
Tcrop_1d = [Tmonth*3 + 12:Tstep:Tmonth*3 + 1*24 + 12];
% For 6d
Tstep = 3;
Tcrop_6d = [Tmonth*3+ 3*24:Tstep:Tmonth*3 + 6*24*1 + 3*24];
% For 1m
Tstep = 6;
Tcrop_1m = [Tmonth*3 + Tmonth./2:Tstep:Tmonth*(3+1) + Tmonth./2];
% For 3m
Tstep = 12;
Tcrop_3m = [Tmonth*3 + Tmonth*1.5:Tstep:Tmonth*3*2 + Tmonth.*1.5];

%Xloc = [M-5 M-3 M-1 M];
%Xloc = [M-6 M-3 M];
Xloc = [M-5 M-2 M];
Xloc_st = Xloc - 1;

M_list = {'s','v','<','>','d','x'};
%col_list = [0 0.4470 0.7410; 0.8500 0.3250 0.0980; 0.9290 0.6940 0.1250];
%col_list = [0.4940 0.1840 0.5560; 0.9290 0.6940 0.1250; 0.8500 0.3250 0.0980; 0 0.4470 0.7410];
col_list = [0.9290 0.6940 0.1250; 0.8500 0.3250 0.0980; 0 0.4470 0.7410];
xloc_nb = length(Xloc);

%comp_runs values: 1 = noBF plotted everywhere / 2 = noBF only for Sch
comp_runs = 2;

%%%% To use instead of plotyy
% yyaxis left
% yyaxis right
%i = 1;

    si = 1;
    S1 = subplot(sy,sx,si);
    ii = 1;
    ij = ii + 1;
        for jj = 1:xloc_nb
            vol_fact = 1 ./ (1 - ST_ar(ii).cst.lambda_s);
            %p12(jj) = plot(t_plot(Tcrop_1d),ST_ar(ii).V_a(Tcrop_1d,Xloc(jj))./ST_ar(ii+1).D_i(Tcrop_1d,Xloc(jj)),'Linewidth',LW1);
            %p12(jj) = plot(t_plot(Tcrop_1d),ST_ar(ii).V_a(Tcrop_1d,Xloc(jj)),'Linewidth',LW1);
            p12(jj) = plot(t_plot(Tcrop_1d),ST_ar(ii).V_a(Tcrop_1d,Xloc(jj)).*vol_fact,'Linewidth',LW1);
            hold on
            %p12(jj).LineStyle = '--';
            p12(jj).Color = col_list(jj,:);
            
            a = '@ ';
            b = num2str((Xloc(jj)-1).*ST_ar(ii).cst.dx ./ 1e3);
            c = 'km';
            %Leg_txtA{jj} = strcat({a},{b},{c});
            Leg_txtA{jj} = [a b c];
        end

        %S1.YLim = [0 0.55];
        S1.YLim = [0 0.8];

        if comp_runs == 1
            for jj=1:xloc_nb
                vol_fact = 1 ./ (1 - ST_ar(ii).cst.lambda_s);
                %p11(jj) = plot(t_plot(Tcrop_1d),ST_ar(ij).V_a(Tcrop_1d,Xloc(jj))./ST_ar(ij).D_i(Tcrop_1d,Xloc(jj)),'Linewidth',LW2);
                %p11(jj) = plot(t_plot(Tcrop_1d),ST_ar(ij).V_a(Tcrop_1d,Xloc(jj)),'Linewidth',LW2);
                p11(jj) = plot(t_plot(Tcrop_1d),ST_ar(ij).V_a(Tcrop_1d,Xloc(jj)).*vol_fact,'Linewidth',LW2);
                hold on
                p11(jj).LineStyle = '--';
                p11(jj).Color = col_list(jj,:);
            end
            %S1.YAxis(2).Color = [1 1 1].*0.4; % To just tackle the right axis
            %S1.YLabel.String = 'Uncoupled V_s / D_i (m); dashed';
            %S1.YLabel.String = 'Uncoupled V_s (m^2); dashed';
        end

        %S1.YLabel.String = 'V_s / D_i (m)';
        S1.YLabel.String = 'V_s / (1 - \lambda) (m^2)';

        % KEEP THAT HERE if you want the discharge plotted on the first plot.
        yyaxis right
        p23(1) = plot(t_plot(Tcrop_1d),ST_ar(ii).Qch(Tcrop_1d,Xloc(end)),'Linewidth',LW);
        p23(1).Color = [1 1 1].*0.75;
        S1.YAxis(2).Color = [1 1 1].*0.4; % To just tackle the right axis
        S1.YLim = [45 105];
        S1.YLabel.String = 'Q_{ch}(x=X_L) (m^3/s)';

        %Leg_txtA{jj} = strcat({'@ '},num2str((Xloc(jj)).*ST_ar(ii+1).cst.dx ./ 1e3),'km');
        %Leg_txtA = {'@ km','@ 29.7 km','@ 30 km','@ 29.1 km uncoupled','@ 29.7 km uncoupled','@ 30 km uncoupled'};

        legA = legend(p12,Leg_txtA);
        legA.Orientation = 'horizontal';
        legA.Position(1) = 0.2064;
        legA.Position(2) = 0.9715;
        legA.Box = 'off';

    si = si+1;
    S2 = subplot(sy,sx,si);

        for jj = 1:xloc_nb

            p22(jj) = plot(t_plot(Tcrop_1d),ST_ar(ii).Sch(Tcrop_1d,Xloc(jj)),'Linewidth',LW1);
            p22(jj).Color = col_list(jj,:);
            hold on
        end
        S2.YLabel.String = 'S_{ch} (m^2)';

        if comp_runs == 1 || comp_runs == 2
            for jj = 1:xloc_nb
                p21(jj) = plot(t_plot(Tcrop_1d),ST_ar(ij).Sch(Tcrop_1d,Xloc(jj)),'Linewidth',LW2);
                hold on
                p21(jj).LineStyle = '--';
                p21(jj).Color = col_list(jj,:);

                a2 = '@ ';
                b2 = num2str((Xloc(jj)-1).*ST_ar(ij).cst.dx ./ 1e3);
                c2 = 'km; decoup.';
                %Leg_txtA{jj} = strcat({a},{b},{c});
                Leg_txtB{jj} = [a2 b2 c2];
            end
        end

        legB = legend(p21,Leg_txtB);
        legB.Orientation = 'horizontal';
        legB.Position(1) = 0.1079;
        legB.Position(2) = 0.9492;
        legB.Box = 'off';

        %yyaxis right
        %p23(1) = plot(t_plot(Tcrop_1d),ST_ar(ii+1).Qch(Tcrop_1d,Xloc(end)),'Linewidth',LW2);
        %p23(1).Color = [1 1 1].*0.65;
        %S2.YAxis(2).Color = [1 1 1].*0.4; % To just tackle the right axis
        %S2.YLim = [45 105];
        %S2.YLabel.String = 'Q_{ch} (m^3/2)';

    si = si+1;
    S3 = subplot(sy,sx,si);
    ii = ii + 2;
    ij = ii + 1;
        for jj = 1:xloc_nb
            vol_fact = 1 ./ (1 - ST_ar(ii).cst.lambda_s);
            %p32(jj) = plot(t_plot(Tcrop_6d),ST_ar(ii).V_a(Tcrop_6d,Xloc(jj))./ST_ar(ii+1).D_i(Tcrop_6d,Xloc(jj)),'Linewidth',LW1);
            %p32(jj) = plot(t_plot(Tcrop_6d),ST_ar(ii).V_a(Tcrop_6d,Xloc(jj)),'Linewidth',LW1);
            p32(jj) = plot(t_plot(Tcrop_6d),ST_ar(ii).V_a(Tcrop_6d,Xloc(jj)).*vol_fact,'Linewidth',LW1);
            hold on
            %p32(jj).LineStyle = '--';
            p32(jj).Color = col_list(jj,:);
        end
        %S3.YLabel.String = 'V_s / D_i (m)';
        %S3.YLabel.String = 'V_s (m^2)';

        %S3.YLim = [0 3];
        S3.YLim = [0 4];

        % KEEP THAT HERE if you want the discharge plotted on the first plot.
        yyaxis right
        p33(1) = plot(t_plot(Tcrop_6d),ST_ar(ii).Qch(Tcrop_6d,Xloc(end)),'Linewidth',LW);
        p33(1).Color = [1 1 1].*0.75;
        S3.YAxis(2).Color = [1 1 1].*0.4; % To just tackle the right axis
        S3.YLim = [45 105];
        %S3.YLabel.String = 'Q_{ch}(x=X_L) (m^3/s)';

    si = si+1;
    S4 = subplot(sy,sx,si);

        for jj = 1:xloc_nb
            p42(jj) = plot(t_plot(Tcrop_6d),ST_ar(ii).Sch(Tcrop_6d,Xloc(jj)),'Linewidth',LW1);
            p42(jj).Color = col_list(jj,:);
            hold on
        end
        %S4.YLabel.String = 'S_{ch} (m^2)';

        if comp_runs == 1 || comp_runs == 2
            for jj = 1:xloc_nb
                p41(jj) = plot(t_plot(Tcrop_6d),ST_ar(ij).Sch(Tcrop_6d,Xloc(jj)),'Linewidth',LW2);
                hold on
                p41(jj).LineStyle = '--';
                p41(jj).Color = col_list(jj,:);
            end
        end


    si = si+1;
    S5 = subplot(sy,sx,si);
    ii = ii + 2;
    ij = ii + 1;
        for jj = 1:xloc_nb
            vol_fact = 1 ./ (1 - ST_ar(ii).cst.lambda_s);
            %p52(jj) = plot(t_plot(Tcrop_1m),ST_ar(ii).V_a(Tcrop_1m,Xloc(jj))./ST_ar(ii+1).D_i(Tcrop_1m,Xloc(jj)),'Linewidth',LW1);
            %p52(jj) = plot(t_plot(Tcrop_1m),ST_ar(ii).V_a(Tcrop_1m,Xloc(jj)),'Linewidth',LW1);
            p52(jj) = plot(t_plot(Tcrop_1m),ST_ar(ii).V_a(Tcrop_1m,Xloc(jj)).*vol_fact,'Linewidth',LW1);
            hold on
            %p52(jj).LineStyle = '--';
            p52(jj).Color = col_list(jj,:);
        end
        %S5.YLabel.String = 'V_s / D_i (m)';
        %S5.YLabel.String = 'V_s (m^2)';

        %S5.YLim = [0 5];
        S5.YLim = [0 7];

        % KEEP THAT HERE if you want the discharge plotted on the first plot.
        yyaxis right
        p53(1) = plot(t_plot(Tcrop_1m),ST_ar(ii).Qch(Tcrop_1m,Xloc(end)),'Linewidth',LW);
        p53(1).Color = [1 1 1].*0.75;
        S5.YAxis(2).Color = [1 1 1].*0.4; % To just tackle the right axis
        S5.YLim = [45 105];
        %S5.YLabel.String = 'Q_{ch}(x=X_L) (m^3/s)';

    si = si+1;
    S6 = subplot(sy,sx,si);

        for jj = 1:xloc_nb

            p62(jj) = plot(t_plot(Tcrop_1m),ST_ar(ii).Sch(Tcrop_1m,Xloc(jj)),'Linewidth',LW1);
            p62(jj).Color = col_list(jj,:);
            hold on
        end
        %S6.YLabel.String = 'S_{ch} (m^2)';

        if comp_runs == 1 || comp_runs == 2
            for jj = 1:xloc_nb
                p61(jj) = plot(t_plot(Tcrop_1m),ST_ar(ij).Sch(Tcrop_1m,Xloc(jj)),'Linewidth',LW2);
                hold on
                p61(jj).LineStyle = '--';
                p61(jj).Color = col_list(jj,:);
            end
        end


    si = si+1;
    S7 = subplot(sy,sx,si);
    ii = ii + 2;
    ij = ii + 1;
        for jj = 1:xloc_nb
            vol_fact = 1 ./ (1 - ST_ar(ii).cst.lambda_s);
            %p72(jj) = plot(t_plot(Tcrop_3m),ST_ar(ii).V_a(Tcrop_3m,Xloc(jj))./ST_ar(ii+1).D_i(Tcrop_3m,Xloc(jj)),'Linewidth',LW1);
            %p72(jj) = plot(t_plot(Tcrop_3m),ST_ar(ii).V_a(Tcrop_3m,Xloc(jj)),'Linewidth',LW1);
            p72(jj) = plot(t_plot(Tcrop_3m),ST_ar(ii).V_a(Tcrop_3m,Xloc(jj)).*vol_fact,'Linewidth',LW1);
            hold on
            %p72(jj).LineStyle = '--';
            p72(jj).Color = col_list(jj,:);

        end
        %S7.YLabel.String = 'V_s / D_i (m)';
        %S7.YLabel.String = 'V_s (m^2)';

        %S7.YLim = [0.025 0.055];
        S7.YLim = [0.04 0.08];

        % KEEP THAT HERE if you want the discharge plotted on the first plot.
        yyaxis right
        p73(1) = plot(t_plot(Tcrop_3m),ST_ar(ii).Qch(Tcrop_3m,Xloc(end)),'Linewidth',LW);
        p73(1).Color = [1 1 1].*0.75;
        S7.YAxis(2).Color = [1 1 1].*0.4; % To just tackle the right axis
        S7.YLim = [45 105];
        %S7.YLabel.String = 'Q_{ch}(x=X_L) (m^3/s)';

        S7.XLabel.String = 'Time (day)';

    si = si+1;
    S8 = subplot(sy,sx,si);
        for jj = 1:xloc_nb

            p82(jj) = plot(t_plot(Tcrop_3m),ST_ar(ii).Sch(Tcrop_3m,Xloc(jj)),'Linewidth',LW1);
            p82(jj).Color = col_list(jj,:);
            hold on
        end
        %S8.YLabel.String = 'S_{ch} (m^2)';

        if comp_runs == 1 || comp_runs == 2
            for jj = 1:xloc_nb
                p81(jj) = plot(t_plot(Tcrop_3m),ST_ar(ij).Sch(Tcrop_3m,Xloc(jj)),'Linewidth',LW2);
                hold on
                p81(jj).LineStyle = '--';
                p81(jj).Color = col_list(jj,:);
            end
        end

%%%%%%%%%%
% Making shit neat again

S1.FontSize = FSax;
S2.FontSize = FSax;
S3.FontSize = FSax;
S4.FontSize = FSax;
S5.FontSize = FSax;
S6.FontSize = FSax;
S7.FontSize = FSax;
S8.FontSize = FSax;

S2.YAxisLocation = 'right';
S4.YAxisLocation = 'right';
S6.YAxisLocation = 'right';
S8.YAxisLocation = 'right';

% XLims
S1.XLim = [t_plot(Tcrop_1d(1)) t_plot(Tcrop_1d(end))];
S2.XLim = [t_plot(Tcrop_1d(1)) t_plot(Tcrop_1d(end))];

S3.XLim = [t_plot(Tcrop_6d(1)) t_plot(Tcrop_6d(end))];
S4.XLim = [t_plot(Tcrop_6d(1)) t_plot(Tcrop_6d(end))];

S5.XLim = [t_plot(Tcrop_1m(1)) t_plot(Tcrop_1m(end))];
S6.XLim = [t_plot(Tcrop_1m(1)) t_plot(Tcrop_1m(end))];

S7.XLim = [t_plot(Tcrop_3m(1)) t_plot(Tcrop_3m(end))];
S8.XLim = [t_plot(Tcrop_3m(1)) t_plot(Tcrop_3m(end))];

% YLims
YL_Sch = [26 45];
S2.YLim = YL_Sch;
S4.YLim = YL_Sch;
S6.YLim = YL_Sch;
S8.YLim = YL_Sch;

%%%%%%%%%%%%%%%
%%  ADJUSTING THE POSITION OF THE SUBPLOTS

sub_x = 0.11;
S1.Position(1) = sub_x;
S3.Position(1) = sub_x;
S5.Position(1) = sub_x;
S7.Position(1) = sub_x;

sub_w = 0.32;
S1.Position(3) = sub_w;
S2.Position(3) = sub_w;
S3.Position(3) = sub_w;
S4.Position(3) = sub_w;
S5.Position(3) = sub_w;
S6.Position(3) = sub_w;
S7.Position(3) = sub_w;
S8.Position(3) = sub_w;

sub_h = 0.18;
S1.Position(4) = sub_h;
S2.Position(4) = sub_h;
S3.Position(4) = sub_h;
S4.Position(4) = sub_h;
S5.Position(4) = sub_h;
S6.Position(4) = sub_h;
S7.Position(4) = sub_h;
S8.Position(4) = sub_h;



% Adding some minor ticks on subplot
subplot(S1)
S1.XMinorTick = 'on';
S1.XGrid = 'on';
yyaxis left
S1.YMinorTick = 'on';
S1.YGrid = 'on';
yyaxis right
S1.YMinorTick = 'on';

S2.XMinorTick = 'on';
S2.YMinorTick = 'on';
S2.XGrid = 'on';
S2.YGrid = 'on';

subplot(S3)
S3.XMinorTick = 'on';
S3.XGrid = 'on';
yyaxis left
S3.YMinorTick = 'on';
S3.YGrid = 'on';
yyaxis right
S3.YMinorTick = 'on';

S4.XMinorTick = 'on';
S4.YMinorTick = 'on';
S4.XGrid = 'on';
S4.YGrid = 'on';

subplot(S5)
S5.XMinorTick = 'on';
S5.XGrid = 'on';
yyaxis left
S5.YMinorTick = 'on';
S5.YGrid = 'on';
yyaxis right
S5.YMinorTick = 'on';

S6.XMinorTick = 'on';
S6.YMinorTick = 'on';
S6.XGrid = 'on';
S6.YGrid = 'on';

subplot(S7)
S7.XMinorTick = 'on';
S7.XGrid = 'on';
yyaxis left
S7.YMinorTick = 'on';
S7.YGrid = 'on';
yyaxis right
S7.YMinorTick = 'on';

S8.XMinorTick = 'on';
S8.YMinorTick = 'on';
S8.XGrid = 'on';
S8.YGrid = 'on';


%legA.Position(1) = 0.32;
%legA.Position(2) = 0.812;

%legA.Box = 'off';

%Adding text on there
subplot(S1)
%xposfact = 0.05;
xposfact = 0.92;
%xposfact2 = 0.96;
%yposfact = 0.85;
yposfact2 = 0.10;
%tA = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact,S1.YLim(2).*yposfact,'A');
tA = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact,S1.YLim(2) - (S1.YLim(2)-S1.YLim(1)).*yposfact2,'A');
subplot(S2)
%tB = text(S2.XLim(2) - (S2.XLim(2)-S2.XLim(1)).*xposfact,S2.YLim(2).*yposfact,'B');
tB = text(S2.XLim(2) - (S2.XLim(2)-S2.XLim(1)).*xposfact,S2.YLim(2) - (S2.YLim(2)-S2.YLim(1)).*yposfact2,'B');
subplot(S3)
tC = text(S3.XLim(2) - (S3.XLim(2)-S3.XLim(1)).*xposfact,S3.YLim(2) - (S3.YLim(2)-S3.YLim(1)).*yposfact2,'C');


subplot(S4)
tD = text(S4.XLim(2) - (S4.XLim(2)-S4.XLim(1)).*xposfact,S4.YLim(2) - (S4.YLim(2)-S4.YLim(1)).*yposfact2,'D');
subplot(S5)
tE = text(S5.XLim(2) - (S5.XLim(2)-S5.XLim(1)).*xposfact,S5.YLim(2) - (S5.YLim(2)-S5.YLim(1)).*yposfact2,'E');
subplot(S6)
tF = text(S6.XLim(2) - (S6.XLim(2)-S6.XLim(1)).*xposfact,S6.YLim(2) - (S6.YLim(2)-S6.YLim(1)).*yposfact2,'F');
subplot(S7)
tG = text(S7.XLim(2) - (S7.XLim(2)-S7.XLim(1)).*xposfact,S7.YLim(2) - (S7.YLim(2)-S7.YLim(1)).*yposfact2,'G');
subplot(S8)
tH = text(S8.XLim(2) - (S8.XLim(2)-S8.XLim(1)).*xposfact,S8.YLim(2) - (S8.YLim(2)-S8.YLim(1)).*yposfact2,'H');

tA.FontSize = FStxt;
tB.FontSize = FStxt;
tC.FontSize = FStxt;
tD.FontSize = FStxt;
tE.FontSize = FStxt;
tF.FontSize = FStxt;
tG.FontSize = FStxt;
tH.FontSize = FStxt;
