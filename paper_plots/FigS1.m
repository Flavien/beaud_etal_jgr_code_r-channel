% Script to compute the changes in sediment volume in the system
% to test if it is conserved.
%
%

fvol = figure;
fvol.Units = 'centimeters';
fvol.Position(3) = 14;
fvol.Position(4) = 6;

for i=1:3
    if i == 1
        load runs/up_q_b_WDG30_dt450s.mat;
        S1 = subplot(1,3,1);
    elseif i == 2
        load runs/up_q_b_WDG30_dt900s.mat;
        S2 = subplot(1,3,2);
    elseif i == 3
        load runs/up_q_b_WDG30_dt3600s.mat;
        S3 = subplot(1,3,3);
    end
    what_you_need_in_ode_aftermath

    %Calculating the sediment volume in the whos system at any time
    cut_x = 10;
    vol_cell = store_var.V_a.*cst.dx;
    %sed_vol_tot = trapz(vol_cell');
    %sed_vol_cut = trapz(vol_cell(:,cut_x:end)');
    sed_vol_tot = sum(vol_cell');
    sed_vol_cut = sum(vol_cell(:,cut_x:end)');

    dvol_tot = (sed_vol_tot(2:end) - sed_vol_tot(1:end-1));
    dvol_cut = (sed_vol_cut(2:end) - sed_vol_cut(1:end-1));

    dt_fact = 3600 ./ cst.dt; %Multiplier needed to get time in hours
    time_vect = linspace(1,cst.tstp_nb,cst.tstp_nb)./dt_fact;
    time_vect_avg = (time_vect(2:end) + time_vect(1:end-1))./2;


        if sim_cond.upstream_qs_flux == 1
            if sim_cond.flux_per_uw ==1 
                flux_diff = D_i(:,1).*model_var.sed_flux - D_i(:,end).*q_b(:,end);
            elseif sim_cond.flux_per_uw == 0 
                flux_diff = model_var.sed_flux - D_i(:,end).*q_b(:,end);
            end
            %flux_diff = model_var.sed_flux - store_q_tt(:,end);
        else
            %p1 = plot(-q_tc_eta(:,end).*cst.dt.*D_i(:,end),'o-');
            %p1 = plot(-q_b(:,end).*cst.dt.*D_i(:,end),'o-');
            flux_diff = -q_b(:,end) .* D_i(:,end);
        end
        flux_diff_avg = (flux_diff(2:end) + flux_diff(1:end-1)) ./ 2;
        p1 = plot(time_vect_avg,flux_diff_avg'.*cst.dt,'o-');
        %p1 = plot(flux_diff.*cst.dt,'o-');

        hold on
        p2 = plot(time_vect_avg,dvol_tot,'x-');
        ax = gca;

        l1 = legend('V_{bal} (m^3)','\Delta V_s (m^3)');
        l1.Box = 'off';

        ax.YLabel.String = 'Volume change between timesteps'; 
        ax.XLabel.String = 'Time since simulation start (hr)'; 


    %%%%%%%%%%%%%
    % Making it all nice and tidy    
    LW = 1.5;
    FS = 8;
    MS = 8;

    p1.LineWidth = LW;
    p2.LineWidth = LW;

    p1.MarkerSize = MS;
    p2.MarkerSize = MS;

    ax.FontSize = FS;

    clearvars -except i
end

