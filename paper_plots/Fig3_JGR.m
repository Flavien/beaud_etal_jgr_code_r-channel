% File to plot stuff from the simulations where I aim at getting to a steady state system
% 
%
% NOTE: apparently to work around the issue of dashed lines being weird, on can go an .eps file
% open it in a editor and change the [X X] 0 setdash lines
% the first X is dash length and second is space length
% for ex run:
% :%s/\[10 6\] 0 setdash/\[3 1.3\] 0 setdash/g
%
% before saveas(f1,'paper_figures/SS_up_q_b_comp_dx100','epsc')
% is useful
%
% To do before running:
% vi ../load_and_prep/load_comp_runs.m
% uncomment
% run_list = list_SS_target;
% update path
% run
% ST_ar = load_comp_runs;


close all

dx = ST_ar(1).cst.dx;
M = ST_ar(1).cst.M;
dt = ST_ar(1).cst.dt;

x_stag = linspace(dx./2,dx.*(M-1)-dx./2,M-1).*1e-3;
x_plot = linspace(0,dx.*(M-1),M).*1e-3;

x_span = [M-6:M];
x_span2 = [M-6:M-1];

%last_day = 328;
last_day = 248;
frst_day = 25;
hr_in_dt = dt./3600;
t_slice_end = last_day * 24 ./ hr_in_dt;
t_through = 3108 - 132*2;
t_slice_st = frst_day * 24 ./ hr_in_dt;

% For V_a time series
t_slice = [t_slice_st:1:t_slice_end];
% For q_ttc time series
t_slice2 = [t_slice_st:1:t_slice_end];

sy = 3;
sx = 2;

LW = 1.2;
MS = 4;
MS2 = 0.001;
FS = 8;

%plot_nb = length(ST_ar);
plot_nb = 4;

M_list = {'s','v','d','o','x','>'};

%l_txt_ar = {'R1; q_{ls}(x=0)=0.1','R2; q_{ls}(x=0)=0.12','R3; q_{ls}(x=0)=0.13','R4; q_{ls}(x=0)=0.14','R5; q_{ls}(x=0)=0.15','R6; q_{ls}(x=0)=0.13'};
%l_txt_ar = {'R1; q_{ls}(x=0)=0.200','R2; q_{ls}(x=0)=0.205','R3; q_{ls}(x=0)=0.210','R4; q_{ls}(x=0)=0.215',...
l_txt_arA = {'R1; q_{ls}(x=0)=0.200','R2; q_{ls}(x=0)=0.205','R3; q_{ls}(x=0)=0.210','R4; q_{ls}(x=0)=0.215',...
    'R4 @ x=X_L-dx'};

l_txt_arB = {'R1','R2','R3','R4','R4 @ t=237 days'};

f1 = figure;
f1.Units = 'centimeters';
f1.Position(1) = 2;
f1.Position(3) = 15;
f1.Position(4) = 14;

    si = 1;
    S1 = subplot(sy,sx,si);

        t_slice_day_fact = hr_in_dt./24;
        for i = 1:plot_nb
            %max_Va(i) = max(ST_ar(i).V_a(t_slice,end));
            max_Va(i) = 1;
            p1a(i) = plot(t_slice.*t_slice_day_fact,ST_ar(i).V_a(t_slice,end)./max_Va(i),'LineWidth',LW);
            %p1a(i).Marker = M_list{i};
            hold on
        end
        % to plot one node up-glacier for the last simulation
        p1b(i) = plot(t_slice.*t_slice_day_fact,ST_ar(i).V_a(t_slice,end-1)./max_Va(i),'LineWidth',LW);
        p1b(i).LineStyle = '--';
        p1b(i).Color = p1a(i).Color;

        %S1.YLabel.String = 'Normalized V_{s}(x=X_L)';
        S1.YLabel.String = 'V_{s}(x=X_L) (m^2)';
        S1.XLabel.String = 'Time during simulation (day)';

        l1 = legend([p1a(1:2)],l_txt_arA(1:2));

    si = si + 1;
    S5 = subplot(sy,sx,si);
        x_loc = M;
        for i = 1:plot_nb
            p5a(i) = plot(t_slice2.*t_slice_day_fact,ST_ar(i).q_ttc(t_slice2,x_loc),'LineWidth',LW);
            hold on
        end
        % to plot one node up-glacier for the last simulation
        p5b(i) = plot(t_slice2.*t_slice_day_fact,ST_ar(i).q_ttc(t_slice2,x_loc-1),'LineWidth',LW);
        p5b(i).LineStyle = '--';
        p5b(i).Color = p5a(i).Color;

        S5.YLabel.String = 'q_{ttc}(x=X_L) (m^3/s)';
        S5.XLabel.String = 'Time during simulation (day)';

        S5.XLim = [t_slice2(1) t_slice2(end)];
        %S5.YLim = [0.12 0.16];

        l5 = legend([p5a(3:4) p5b(i)],l_txt_arA(3:5));

    si = si + 1;
    S2 = subplot(sy,sx,si);

        %for i = 1:plot_nb-1
        for i = 1:plot_nb
            p2a(i) = plot(x_plot(x_span),ST_ar(i).V_a(t_slice_end,x_span),'LineWidth',LW,'MarkerSize',MS);
            p2a(i).Marker = M_list{i};
            hold on
        end
        % to plot one node up-glacier for the last simulation
        p2b(i) = plot(x_plot(x_span),ST_ar(i).V_a(t_through,x_span),'LineWidth',LW,'MarkerSize',MS);
        p2b(i).LineStyle = '--';
        p2b(i).Marker = M_list{i+1};
        p2b(i).Color = p2a(i).Color;

        S2.YLabel.String = strcat('V_{s} (t=',num2str(last_day),' days) (m^2)');

        l2 = legend(l_txt_arB);

    si = si + 1;
    S3 = subplot(sy,sx,si);
        for i = 1:plot_nb
            p3a(i) = plot(x_plot(x_span),ST_ar(i).Sch(t_slice_end,x_span),'LineWidth',LW,'MarkerSize',MS);
            p3a(i).Marker = M_list{i};
            hold on
        end
        % to plot one node up-glacier for the last simulation
        p3b(i) = plot(x_plot(x_span),ST_ar(i).Sch(t_through,x_span),'LineWidth',LW,'MarkerSize',MS);
        p3b(i).LineStyle = '--';
        p3b(i).Marker = M_list{i+1};
        p3b(i).Color = p3a(i).Color;

        S3.YLabel.String = strcat('S_{ch} (t=',num2str(last_day),' days) (m^2)');

        %l2 = legend(l_txt_ar(1:plot_nb));
        %l2 = legend(l_txt_ar(1:plot_nb+1));
        %l2.Location = 'NorthWest';
        %l2.Box = 'off';


    si = si + 1;
    S6 = subplot(sy,sx,si);
        for i = 1:plot_nb
            p6a(i) = plot(x_stag(x_span2),abs(ST_ar(i).dphicdx(t_slice_end,x_span2)),'LineWidth',LW,'MarkerSize',MS);
            p6a(i).Marker = M_list{i};
            hold on
        end
        % to plot one node up-glacier for the last simulation
        p6b(i) = plot(x_stag(x_span2),abs(ST_ar(i).dphicdx(t_through,x_span2)),'LineWidth',LW,'MarkerSize',MS);
        p6b(i).LineStyle = '--';
        p6b(i).Marker = M_list{i+1};
        p6b(i).Color = p6a(i).Color;

        S6.XLabel.String = 'Distance from water source (km)';
        %S6.YLabel.String = '|d \phi_{ch} /dx| (Pa/m)';
        S6.YLabel.String = strcat('|d \phi_{ch} /dx| (t=',num2str(last_day),' days) (Pa/m)');

    si = si + 1;
    S4 = subplot(sy,sx,si);
        for i = 1:plot_nb
            p4a(i) = plot(x_plot(x_span),ST_ar(i).q_ttc(end,x_span),'LineWidth',LW,'MarkerSize',MS);
            p4a(i).Marker = M_list{i};
            hold on
        end
        % to plot one node up-glacier for the last simulation
        p4b(i) = plot(x_plot(x_span),ST_ar(i).q_ttc(end,x_span),'LineWidth',LW,'MarkerSize',MS);
        p4b(i).LineStyle = '--';
        p4b(i).Marker = M_list{i+1};
        p4b(i).Color = p4a(i).Color;

        S4.XLabel.String = 'Distance from water source (km)';
        %S4.YLabel.String = 'q_{ttc} (m^3/s)';
        S4.YLabel.String = strcat('q_{ttc} (t=',num2str(last_day),' days) (m^3/s)');

%%%%%%%%%%%%%%%
% The usual of making things nice

S1.XLim = [frst_day last_day];
S5.XLim = [frst_day last_day];

S1.FontSize = FS;
S2.FontSize = FS;
S3.FontSize = FS;
S4.FontSize = FS;
S5.FontSize = FS;
S6.FontSize = FS;

S1.YLim = [0 2.4];
S5.YLim = [0.199 0.216];
S2.YLim = [0 2.4];
S3.YLim = [25.75 27.5];
S6.YLim = [64 74];
S4.YLim = [0.199 0.26];

% Making the subplots the right size
sub_w = 0.38;
sub_h = 0.24;
S1.Position(3:4) = [sub_w sub_h];
S2.Position(3:4) = [sub_w sub_h];
S3.Position(3:4) = [sub_w sub_h];
S4.Position(3:4) = [sub_w sub_h];
S5.Position(3:4) = [sub_w sub_h];
S6.Position(3:4) = [sub_w sub_h];

x_left = 0.10;
S6.Position(1) = x_left;
S1.Position(1) = x_left;
S2.Position(1) = x_left;

x_right = 0.59;
S4.Position(1) = x_right;
S5.Position(1) = x_right;
S3.Position(1) = x_right;

y_1 = 0.685;
S1.Position(2) = y_1;
S5.Position(2) = y_1;

y_2 = 0.37;
S2.Position(2) = y_2;
S3.Position(2) = y_2;

y_3 = 0.08;
S6.Position(2) = y_3;
S4.Position(2) = y_3;

% leg in S1
l1.Box = 'off';
l1.Orientation = 'horizontal';
l1.Position = [0.2822 0.9585 0.4774 0.0416];

% leg in S5
l5.Box = 'off';
l5.Orientation = 'horizontal';
l5.Position = [0.1953 0.9232 0.6733 0.0416];

% leg in S2
l2.Box = 'off';
l2.Location = 'west';


% Adding some minor ticks on subplot
subplot(S1)
S1.XMinorTick = 'on';
S1.YMinorTick = 'on';
S1.XGrid = 'on';
S1.YGrid = 'on';

S2.XMinorTick = 'on';
S2.YMinorTick = 'on';
S2.XGrid = 'on';
S2.YGrid = 'on';

S3.XMinorTick = 'on';
S3.YMinorTick = 'on';
S3.XGrid = 'on';
S3.YGrid = 'on';

S4.XMinorTick = 'on';
S4.YMinorTick = 'on';
S4.XGrid = 'on';
S4.YGrid = 'on';

S5.XMinorTick = 'on';
S5.YMinorTick = 'on';
S5.XGrid = 'on';
S5.YGrid = 'on';

S6.XMinorTick = 'on';
S6.YMinorTick = 'on';
S6.XGrid = 'on';
S6.YGrid = 'on';


%Adding text on there
subplot(S1)
xposfact = 0.95;
yposfact = 0.13;
tA = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact,S1.YLim(2) - (S1.YLim(2)-S1.YLim(1)).*yposfact,'A');
subplot(S5)
tB = text(S5.XLim(2) - (S5.XLim(2)-S5.XLim(1)).*xposfact,S5.YLim(2) - (S5.YLim(2)-S5.YLim(1)).*yposfact,'B');
subplot(S2)
tC = text(S2.XLim(2) - (S2.XLim(2)-S2.XLim(1)).*xposfact,S2.YLim(2) - (S2.YLim(2)-S2.YLim(1)).*yposfact,'C');
subplot(S3)
tD = text(S3.XLim(2) - (S3.XLim(2)-S3.XLim(1)).*xposfact,S3.YLim(2) - (S3.YLim(2)-S3.YLim(1)).*yposfact,'D');
subplot(S6)
tE = text(S6.XLim(2) - (S6.XLim(2)-S6.XLim(1)).*xposfact,S6.YLim(2) - (S6.YLim(2)-S6.YLim(1)).*yposfact,'E');
subplot(S4)
tF = text(S4.XLim(2) - (S4.XLim(2)-S4.XLim(1)).*xposfact,S4.YLim(2) - (S4.YLim(2)-S4.YLim(1)).*yposfact,'F');

tA.FontSize = FS;
tB.FontSize = FS;
tC.FontSize = FS;
tD.FontSize = FS;
tE.FontSize = FS;
tF.FontSize = FS;
