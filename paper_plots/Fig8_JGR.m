% Function to show the different sedimentary structures
% The idea is to see if we can differentiate between forward, backward or vertical accretion
% Before this function one should run
%
%
% This figure is going to show the dynamics behind different sedimentation regimes
%
% ST_ar = load_comp_run();
%
% To have a look at what the sediment evolution looks like
%
%figure
%imagesc(eta_s(6000:7000,term_slice))

% Redifining some cst to make the coding easier
dx = ST_ar(1).cst.dx;
M = ST_ar(1).cst.M;

% Making the x-axis proper on different grids
x_stag = linspace(dx./2,dx.*(M-1)-dx./2,M-1).*1e-3;
x_plot = linspace(0,dx.*(M-1),M).*1e-3;

%For dx = 100m
term_slice = [280:301];

% Choosing which simulation I am using from the structure array
jj = 1;

% Just recalculating something before plotting
eta_s = (ST_ar(jj).V_a ./ ST_ar(jj).D_i) .* (1./(1-ST_ar(jj).cst.lambda_s));

% Changes in eta_s per timestep
Deta_s = (eta_s(:,2:end) - eta_s(:,1:end-1)); % ./ ST_ar(jj).cst.dt;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Setting up font, lines, and marker sizes
FS = 10;
FSax = 8;
LW = 2;
LWms = 1;
MS = 10;

% Parameters for subplot array
sx = 2;
sy = 2;

% defining different colors from rgb function
% Note that this could be done more concisely by storing colors
% is a matrix rather than different vectors
col1a = rgb('DarkRed');
col1b = rgb('Crimson');
col1c = rgb('DarkSalmon');

col2a = rgb('DarkGreen');
col2b = rgb('Olive');
col2c = rgb('LightSeaGreen');

col3a = rgb('Indigo');
col3b = rgb('DarkOrchid');
col3c = rgb('Orchid');

col_a = [1 1 1].*0.2;
col_b = [1 1 1].*0.4;
col_c = [1 1 1].*0.6;

% Setting some parameters to cut matrices while plotting
sed_start = (185+93.5)*24;
sed_finish = (185+97)*24;
%sed_finish = 6600;
seas_start = 185*24;
seas_end = (185+115)*24;

% To choose which of the simulations I want to use to make the plot
jj = 1;

disp('There is something wrong with how you plotted the time in here! Maybe the offset at the beginning is not quite the same as in previous simulations')
% Defining some more parameters to automatize plotting across different simulations
Q_start = sed_start;
% t_span of plot in days
t_span_seas = ([seas_start+1 : seas_end] - seas_start).*ST_ar(jj).cst.dt ./ 3600 ./ 24 ;
t_span_d = ([sed_start:sed_finish]-seas_start).*ST_ar(jj).cst.dt ./ 3600 ./ 24 ;
t_span_d2 = ([Q_start:sed_finish]-seas_start).*ST_ar(jj).cst.dt ./ 3600 ./ 24 ;

x_min = 28.5;
x_max = 30;

% NOTE ABOUT FIGURE WIDTH FOR AGU PUBLICATIONS
% figure width = 20pc. 1pc = 0.42cm
% 20pc = 8.4 cm 2 col fig width whould be around 16.8cm.
% starting a figure and defining the size depending on journal requirements
f1 = figure;
f1.Units = 'centimeters';
f1.Position(3) = 14;
f1.Position(4) = 11;

% More setting up for plotting
i = 6456;
% 6432 / 6454 56? 
% 6462 64? 
% 6486 / 6510 / 6534
%t_vect = [6456 6462 6534]; % Great for 600 m wedge
%t_vect = [6456 6462+25 6534+25]; % Not sure what it's for...
% Testing different stuff...
%t_vect = [6457 6460+2*24 6461+10*24];
t_vect = [6458+10*24 6461+11*24 6464+12*24];

dt_plot = 2;

% Extracting the position I need to plot the dots
i_pos = (t_vect-seas_start).*ST_ar(jj).cst.dt ./ 3600 ./ 24;
i_posb = (t_vect-dt_plot-seas_start).*ST_ar(jj).cst.dt ./ 3600 ./ 24;
i_posc = (t_vect-dt_plot*2-seas_start).*ST_ar(jj).cst.dt ./ 3600 ./ 24;


% 1st subplot
spos = 1;
S1 = subplot(sx,sy,spos);
    
    p1 = plot(t_span_d2,ST_ar(jj).Qch(Q_start:sed_finish,end));
    hold on
    p1.LineWidth = LW;

    S1.XLim = [t_span_d2(1) t_span_d2(end)];
    S1.YLim = [30 78];

    S1.FontSize = FSax;

    S1.XLabel.String = 'Time (day)';
    S1.YLabel.String = 'Discharge (m^3 s^{-1})';

%%%%%%%%%%%%%%%%%%%%%
%%%     PLOTTING DOTS FOR EACH SUBPLOT

    % FOR S2
    k = 1;
    p_pt1c = plot(i_posc(k),ST_ar(jj).Qch(t_vect(k)-dt_plot*2,end),'o','MarkerSize',MS,'Color',col1c);
    p_pt1b = plot(i_posb(k),ST_ar(jj).Qch(t_vect(k)-dt_plot,end),'o','MarkerSize',MS,'Color',col1b);
    p_pt1a = plot(i_pos(k),ST_ar(jj).Qch(t_vect(k),end),'o','MarkerSize',MS,'Color',col1a);

    p_pt1a.LineWidth = LWms;
    p_pt1b.LineWidth = LWms;
    p_pt1c.LineWidth = LWms;
    
    p_pt1a.MarkerFaceColor = p_pt1a.Color;
    p_pt1b.MarkerFaceColor = p_pt1b.Color;
    p_pt1c.MarkerFaceColor = p_pt1c.Color;

    % FOR S3
    k = 2;
    p_pt2c = plot(i_posc(k),ST_ar(jj).Qch(t_vect(k)-dt_plot*2,end),'o','MarkerSize',MS,'Color',col2c);
    p_pt2b = plot(i_posb(k),ST_ar(jj).Qch(t_vect(k)-dt_plot,end),'o','MarkerSize',MS,'Color',col2b);
    p_pt2a = plot(i_pos(k),ST_ar(jj).Qch(t_vect(k),end),'o','MarkerSize',MS,'Color',col2a);

    p_pt2a.LineWidth = LWms;
    p_pt2b.LineWidth = LWms;
    p_pt2c.LineWidth = LWms;
    
    p_pt2a.MarkerFaceColor = p_pt2a.Color;
    p_pt2b.MarkerFaceColor = p_pt2b.Color;
    p_pt2c.MarkerFaceColor = p_pt2c.Color;

    % FOR S3
    k = 3;
    p_pt3c = plot(i_posc(k),ST_ar(jj).Qch(t_vect(k)-dt_plot*2,end),'o','MarkerSize',MS,'Color',col3c);
    p_pt3b = plot(i_posb(k),ST_ar(jj).Qch(t_vect(k)-dt_plot,end),'o','MarkerSize',MS,'Color',col3b);
    p_pt3a = plot(i_pos(k),ST_ar(jj).Qch(t_vect(k),end),'o','MarkerSize',MS,'Color',col3a);

    p_pt3a.LineWidth = LWms;
    p_pt3b.LineWidth = LWms;
    p_pt3c.LineWidth = LWms;
    
    p_pt3a.MarkerFaceColor = p_pt3a.Color;
    p_pt3b.MarkerFaceColor = p_pt3b.Color;
    p_pt3c.MarkerFaceColor = p_pt3c.Color;

% 2nd subplot
spos = spos + 1;
S2 = subplot(sx,sy,spos);
        k =1;
        p2c = plot(x_plot(term_slice),eta_s(t_vect(k)-dt_plot*2,term_slice),'Color',col1c,'LineWidth',LW);
        hold on
        p2b = plot(x_plot(term_slice),eta_s(t_vect(k)-dt_plot,term_slice),'Color',col1b,'LineWidth',LW);
        p2a = plot(x_plot(term_slice),eta_s(t_vect(k),term_slice),'Color',col1a,'LineWidth',LW);

        % Trying to put the sediment flux now...
        yyaxis right
        p2e = plot(x_plot(term_slice),...
                ST_ar(jj).q_tt(t_vect(k)-dt_plot*2,term_slice),'LineStyle','--','Color',col_c,'LineWidth',LW);
        p2e = plot(x_plot(term_slice),...
                ST_ar(jj).q_tt(t_vect(k)-dt_plot,term_slice),'LineStyle','--','Color',col_b,'LineWidth',LW);
        p2d = plot(x_plot(term_slice),...
                ST_ar(jj).q_tt(t_vect(k),term_slice),'LineStyle','--','Color',col_a,'LineWidth',LW);
        
        S2.YLabel.String = 'q_{tt} (m^3/s)';
        S2.YAxis(2).Color = [1 1 1].*0.3;
        S2.YLim = [0 0.09];
        S2.YTick = [0 0.03 0.06 0.09];
        yyaxis left

        S2.FontSize = FSax;

        
% 3th subplot
spos = spos + 1;
S3 = subplot(sx,sy,spos);
        k =2;
        p3c = plot(x_plot(term_slice),eta_s(t_vect(k)-dt_plot*2,term_slice),'Color',col2c,'LineWidth',LW);
        hold on
        p3b = plot(x_plot(term_slice),eta_s(t_vect(k)-dt_plot,term_slice),'Color',col2b,'LineWidth',LW);
        p3a = plot(x_plot(term_slice),eta_s(t_vect(k),term_slice),'Color',col2a,'LineWidth',LW);

        % Trying to put the sediment flux now...
        yyaxis right
        p3e = plot(x_plot(term_slice),...
                ST_ar(jj).q_tt(t_vect(k)-dt_plot*2,term_slice),'LineStyle','--','Color',col_c,'LineWidth',LW);
        p3e = plot(x_plot(term_slice),...
                ST_ar(jj).q_tt(t_vect(k)-dt_plot,term_slice),'LineStyle','--','Color',col_b,'LineWidth',LW);
        p3d = plot(x_plot(term_slice),...
                ST_ar(jj).q_tt(t_vect(k),term_slice),'LineStyle','--','Color',col_a,'LineWidth',LW);
        
        S3.YLabel.String = 'q_{tt} (m^3/s)';
        S3.YAxis(2).Color = [1 1 1].*0.3;
        S3.YLim = [0 0.09];
        S3.YTick = [0 0.03 0.06 0.09];
        yyaxis left

        S3.FontSize = FSax;

% 4th subplot
spos = spos + 1;
S4 = subplot(sx,sy,spos);
        k =3;
        p4c = plot(x_plot(term_slice),eta_s(t_vect(k)-dt_plot*2,term_slice),'Color',col3c,'LineWidth',LW);
        hold on
        p4b = plot(x_plot(term_slice),eta_s(t_vect(k)-dt_plot,term_slice),'Color',col3b,'LineWidth',LW);
        p4a = plot(x_plot(term_slice),eta_s(t_vect(k),term_slice),'Color',col3a,'LineWidth',LW);

        % Trying to put the sediment flux now...
        yyaxis right
        p4e = plot(x_plot(term_slice),...
                ST_ar(jj).q_tt(t_vect(k)-dt_plot*2,term_slice),'LineStyle','--','Color',col_c,'LineWidth',LW);
        p4e = plot(x_plot(term_slice),...
                ST_ar(jj).q_tt(t_vect(k)-dt_plot,term_slice),'LineStyle','--','Color',col_b,'LineWidth',LW);
        p4d = plot(x_plot(term_slice),...
                ST_ar(jj).q_tt(t_vect(k),term_slice),'LineStyle','--','Color',col_a,'LineWidth',LW);
        
        S4.YLabel.String = 'q_{tt} (m^3/s)';
        S4.YAxis(2).Color = [1 1 1].*0.3;
        S4.YLim = [0 0.09];
        S4.YTick = [0 0.03 0.06 0.09];
        yyaxis left

        S4.FontSize = FSax;


%%%%%%%%%%%%%%%%%%%
%%%     MAKING EVERYTHING LOOK PRETTY

S2.XLabel.String = 'Distance from water source (km)';
S2.YLabel.String = 'Sed. thickness (m)';

S3.XLabel.String = 'Distance from water source (km)';
S3.YLabel.String = 'Sed. thickness (m)';

S4.XLabel.String = 'Distance from water source (km)';
S4.YLabel.String = 'Sed. thickness (m)';


S2.YLim = [0 0.6];
S2.XLim = [x_min x_max];

S3.YLim = [0 0.6];
S3.XLim = [x_min x_max];

S4.YLim = [0 0.6];
S4.XLim = [x_min x_max];

%S2.YTick = [0 0.05 0.1 0.15 0.2];
%S3.YTick = [0 0.05 0.1 0.15 0.2];
%S4.YTick = [0 0.2 0.4 0.6];

S1.YGrid = 'on';
S2.YGrid = 'on';
S3.YGrid = 'on';
S4.YGrid = 'on';

S1.XGrid = 'on';
S2.XGrid = 'on';
S3.XGrid = 'on';
S4.XGrid = 'on';

% Adjusting the size of the subplots
sub_w = 0.32;
S1.Position(3) = sub_w;
S2.Position(3) = sub_w;
S3.Position(3) = sub_w;
S4.Position(3) = sub_w;

S1.Position(1) = 0.10;
S3.Position(1) = 0.10;

S2.Position(1) = 0.58;
S4.Position(1) = 0.58;

% Adding text label for each subplot
% This way the location is a function of ratios of the ;limits of hte axes on the plot and not a 
% ratio of the total length of the vector.
subplot(S1)
%xposfact = 0.15;
xposfact = 0.95;
%xposfact2 = 0.96;
%yposfact2 = 0.85;
yposfact2 = 0.15;
tA = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact,S1.YLim(2) - (S1.YLim(2)-S1.YLim(1)).*yposfact2,'A');
subplot(S2)
tB = text(S2.XLim(2) - (S2.XLim(2)-S2.XLim(1)).*xposfact,S2.YLim(2) - (S2.YLim(2)-S2.YLim(1)).*yposfact2,'B');
subplot(S3)
tC = text(S3.XLim(2) - (S3.XLim(2)-S3.XLim(1)).*xposfact,S3.YLim(2) - (S3.YLim(2)-S3.YLim(1)).*yposfact2,'C');
subplot(S4)
tD = text(S4.XLim(2) - (S4.XLim(2)-S4.XLim(1)).*xposfact,S4.YLim(2) - (S4.YLim(2)-S4.YLim(1)).*yposfact2,'D');

tA.FontSize = FS;
tB.FontSize = FS;
tC.FontSize = FS;
tD.FontSize = FS;


%%%%%%%%%%%%%%%%
%%  LET'S MAKE A PROPER MESS

% Here I'm addding the axes for the inset in the subplot
% I felt easier to do this at the end after the formatting is achieved, because some of the units are relative
% the figure
    inset_axes = axes('Position',[0.2629 0.8176 0.2098 0.1529]);
    p_in = plot(t_span_seas,ST_ar(jj).Qch(seas_start + 1:seas_end,end));

    inset_axes.XLim = [0 115];
    inset_axes.YLim = [0 210];

    lx = [90 90 100 100 90];
    ly = [20 110 110 20 20];
    l1 = line(lx,ly);

    p_in.LineWidth = 1;
    l1.LineWidth = 1.5;
