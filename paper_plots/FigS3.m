%Script to plot the results obtained with different grid sizes
%
%
%
%
%FIrst run a script to make a structure with the sims that you want to compare
% ST_ar = load_comp_runs();

% Let's start with the upstream flux scenario

us_flux_test    = 1;
init_05m        = 0;
init_1m         = 0;

sim_nb = length(ST_ar);

x_stag1 = linspace(ST_ar(1).cst.dx./2,ST_ar(1).cst.dx.*...
                    (ST_ar(1).cst.M-1)-ST_ar(1).cst.dx./2,ST_ar(1).cst.M-1).*1e-3;
x_plot1 = linspace(0,ST_ar(1).cst.dx.*(ST_ar(1).cst.M-1),ST_ar(1).cst.M).*1e-3;

x_stag2 = linspace(ST_ar(2).cst.dx./2,ST_ar(2).cst.dx.*...
                    (ST_ar(2).cst.M-1)-ST_ar(2).cst.dx./2,ST_ar(2).cst.M-1).*1e-3;
x_plot2 = linspace(0,ST_ar(2).cst.dx.*(ST_ar(2).cst.M-1),ST_ar(2).cst.M).*1e-3;

x_stag3 = linspace(ST_ar(3).cst.dx./2,ST_ar(3).cst.dx.*...
                    (ST_ar(3).cst.M-1)-ST_ar(3).cst.dx./2,ST_ar(3).cst.M-1).*1e-3;
x_plot3 = linspace(0,ST_ar(3).cst.dx.*(ST_ar(3).cst.M-1),ST_ar(3).cst.M).*1e-3;

x_stag4 = linspace(ST_ar(4).cst.dx./2,ST_ar(4).cst.dx.*...
                    (ST_ar(4).cst.M-1)-ST_ar(4).cst.dx./2,ST_ar(4).cst.M-1).*1e-3;
x_plot4 = linspace(0,ST_ar(4).cst.dx.*(ST_ar(4).cst.M-1),ST_ar(4).cst.M).*1e-3;

x_stag5 = linspace(ST_ar(5).cst.dx./2,ST_ar(5).cst.dx.*...
                    (ST_ar(5).cst.M-1)-ST_ar(5).cst.dx./2,ST_ar(5).cst.M-1).*1e-3;
x_plot5 = linspace(0,ST_ar(5).cst.dx.*(ST_ar(5).cst.M-1),ST_ar(5).cst.M).*1e-3;

x_stag6 = linspace(ST_ar(6).cst.dx./2,ST_ar(6).cst.dx.*...
                    (ST_ar(6).cst.M-1)-ST_ar(6).cst.dx./2,ST_ar(6).cst.M-1).*1e-3;
x_plot6 = linspace(0,ST_ar(6).cst.dx.*(ST_ar(6).cst.M-1),ST_ar(6).cst.M).*1e-3;

x_stag7 = linspace(ST_ar(7).cst.dx./2,ST_ar(7).cst.dx.*...
                    (ST_ar(7).cst.M-1)-ST_ar(7).cst.dx./2,ST_ar(7).cst.M-1).*1e-3;
x_plot7 = linspace(0,ST_ar(7).cst.dx.*(ST_ar(7).cst.M-1),ST_ar(7).cst.M).*1e-3;


LW = 1.5;
%MS = 5;
MS = 0.1;
FSax = 8;
FStxt = 10;

sy = 3;
sx = 1;

f1 = figure;
f1.Units = 'centimeters';
f1.Position(3) = 15;
f1.Position(4) = 13;

    if us_flux_test == 1
        pos_qt = [1 2 3 4];
    elseif init_05m == 1
        pos_qt = [1 10 20 30 100];
    elseif init_1m == 1
        pos_qt = [1 50 100 200];
    end
    
    g_fact1 = 0;
    g_fact2 = 0.3;
    g_fact3 = 0.7;
    g_fact4 = 0.7;
    g_fact5 = 0.3;
    g_fact6 = 0.5;
    g_fact7 = 0.5;

    col1 = rgb('DarkGreen');
    col2 = rgb('MediumSeaGreen');
    col3 = rgb('Chartreuse');
    col4 = rgb('MediumBlue');
    col5 = rgb('DeepSkyBlue');
    col6 = rgb('FireBrick');
    col7 = rgb('Salmon');

    x_plot_mat1 = repmat(x_plot1,length(pos_qt),1);
    x_plot_mat2 = repmat(x_plot2,length(pos_qt),1);
    x_plot_mat3 = repmat(x_plot3,length(pos_qt),1);
    x_plot_mat4 = repmat(x_plot4,length(pos_qt),1);
    x_plot_mat5 = repmat(x_plot5,length(pos_qt),1);
    x_plot_mat6 = repmat(x_plot6,length(pos_qt),1);
    x_plot_mat7 = repmat(x_plot7,length(pos_qt),1);

sb_p = 1;
S1 = subplot(sy,sx,sb_p);
    p1a = plot(x_plot_mat1',ST_ar(1).V_a(pos_qt.*4,:)','LineWidth',LW,'Color',col1);
    hold on
    p1b = plot(x_plot_mat2',ST_ar(2).V_a(pos_qt.*4,:)','LineWidth',LW,'Color',col2);
    if init_1m == 0
        p1c = plot(x_plot_mat3',ST_ar(3).V_a(pos_qt.*4,:)','LineWidth',LW,'Color',col3);
        p1d = plot(x_plot_mat4',ST_ar(4).V_a(pos_qt.*4,:)','LineWidth',LW,'Color',col4);
        p1e = plot(x_plot_mat5',ST_ar(5).V_a(pos_qt.*4,:)','LineWidth',LW,'Color',col5);
        p1f = plot(x_plot_mat6',ST_ar(6).V_a(pos_qt.*4,:)','LineWidth',LW,'Color',col6);
        p1g = plot(x_plot_mat7',ST_ar(7).V_a(pos_qt.*4,:)','LineWidth',LW,'Color',col7);
    end

    S1.YLabel.String = 'V_{s} (m^3/m)';
    %S1.XLabel.String = 'Distance from water source (km)';
    %S1.Title.String = strcat('dt =',num2str(ST_ar(1).cst.dt),' s');


    if init_05m == 1
        %ax.YLim = [0 3.8];
        S1.YLim = [0 8.2];
    elseif init_1m == 1
        S1.YLim = [0 7];
    end
     grid on

sb_p = sb_p + 1;
S2 = subplot(sy,sx,sb_p);
    p2a = plot(x_plot1,ST_ar(1).Sch(end,:),'LineWidth',LW,'Color',col1);
    hold on
    p2b = plot(x_plot2,ST_ar(2).Sch(end,:)','LineWidth',LW,'Color',col2);
    p2c = plot(x_plot3,ST_ar(3).Sch(end,:)','LineWidth',LW,'Color',col3);
    p2d = plot(x_plot4,ST_ar(4).Sch(end,:)','LineWidth',LW,'Color',col4);
    p2e = plot(x_plot5,ST_ar(5).Sch(end,:)','LineWidth',LW,'Color',col5);
    p2f = plot(x_plot6,ST_ar(6).Sch(end,:)','LineWidth',LW,'Color',col6);
    p2g = plot(x_plot7,ST_ar(7).Sch(end,:)','LineWidth',LW,'Color',col7);

    S2.YLabel.String = 'S_{ch} (m^2)';
    %S2.XLabel.String = 'Distance from water source (km)';

    if init_1m == 0
        %l1 = legend([p1a(1) p1b(1) p1c(1) p1d(1) p1e(1) p1f(1) p1g(1)],...
        l1 = legend([p2a(1) p2b(1) p2c(1) p2d(1) p2e(1) p2f(1) p2g(1)],...
            'dx = 600m','dx = 300m','dx = 150m','dx = 100m','dx = 50m','dx = 25m','dx = 10m');
        l1.Location = 'SouthWest';
    elseif init_1m == 1
        l1 = legend([p1a(1) p1b(1)],'dx = 900s','dt = 1800s');
    end

    grid on
    
sb_p = sb_p + 1;
S3 = subplot(sy,sx,sb_p);
    p3a = plot(x_plot1,ST_ar(1).q_tc(end,:),'LineWidth',LW,'Color',col1);
    hold on
    p3b = plot(x_plot2,ST_ar(2).q_tc(end,:)','LineWidth',LW,'Color',col2);
    p3c = plot(x_plot3,ST_ar(3).q_tc(end,:)','LineWidth',LW,'Color',col3);
    p3d = plot(x_plot4,ST_ar(4).q_tc(end,:)','LineWidth',LW,'Color',col4);
    p3e = plot(x_plot5,ST_ar(5).q_tc(end,:)','LineWidth',LW,'Color',col5);
    p3f = plot(x_plot6,ST_ar(6).q_tc(end,:)','LineWidth',LW,'Color',col6);
    p3g = plot(x_plot7,ST_ar(7).q_tc(end,:)','LineWidth',LW,'Color',col7);

    S3.YLabel.String = 'q_{tc} (m^2)';
    S3.XLabel.String = 'Distance from water source (km)';

    grid on

% Making these little plots all neat
S1.FontSize = FSax;
S2.FontSize = FSax;
S3.FontSize = FSax;

S1.XMinorTick = 'on';
S1.YMinorTick = 'on';
S2.XMinorTick = 'on';
S2.YMinorTick = 'on';
S3.XMinorTick = 'on';
S3.YMinorTick = 'on';

l1.Box = 'off';

%Adding text on there
subplot(S1)
%xposfact = 0.05;
xposfact = 0.97;
%xposfact2 = 0.96;
yposfact = 0.85;
yposfact2 = 0.15;
tA = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact,S1.YLim(2) - (S1.YLim(2)-S1.YLim(1)).*yposfact2,'A');
subplot(S2)
tB = text(S2.XLim(2) - (S2.XLim(2)-S2.XLim(1)).*xposfact,S2.YLim(2) - (S2.YLim(2)-S2.YLim(1)).*yposfact2,'B');
subplot(S3)
tC = text(S3.XLim(2) - (S3.XLim(2)-S3.XLim(1)).*xposfact,S3.YLim(2) - (S3.YLim(2)-S3.YLim(1)).*yposfact,'C');

tA.FontSize = FStxt;
tB.FontSize = FStxt;
tC.FontSize = FStxt;
