% Let us start with a relatively simple plot to get a sense of the hydraulics that
% we'll be dealing with
%
%
% load runs/up_q_b_WDG30_dt900s_dx100.mat
% what_you_need_in_ode_aftermath

close all

x_stag = linspace(cst.dx./2,cst.dx.*(cst.M-1)-cst.dx./2,cst.M-1).*1e-3;
x_plot = linspace(0,cst.dx.*(cst.M-1),cst.M).*1e-3;

f1 = figure(1);
f1.Units = 'centimeters';
f1.Position(3) = 13;
f1.Position(4) = 7;
sy = 2;
sx = 2;

sb_p = 1;
S1 = subplot(sy,sx,sb_p);

    p1a = plot(x_plot,model_var.H);
    hold on
    p1b = plot(x_plot,model_var.eta_br);

    p1a.Color = 'b';
    p1b.Color = 'k';
    l1 = legend('Ice surface','Bedrock');
    l1.Box = 'off';

    grid on

%sb_p = sb_p +1;
%S2 = subplot(sy,sx,sb_p);

%    p2 = plot(x_plot,Q_c(end,:));

%    S2.YLim = [0 60];

sb_p = sb_p +1;
S3 = subplot(sy,sx,sb_p);

    p3 = plot(x_plot,v_w(end,:));
    
    S3.YAxisLocation = 'right';
    
    grid on


sb_p = sb_p +1;
S4 = subplot(sy,sx,sb_p);

    p4 = plot(x_stag,R_i_avg(end,:).*2);
    
    grid on


sb_p = sb_p +1;
S5 = subplot(sy,sx,sb_p);

    p5 = plot(x_plot,tau_b(end,:));
    
    S5.YAxisLocation = 'right';
    
    grid on


%%%%%%%%%%%%%%%%%%%%%%%
% Making it all tidy
LW = 2;
FS = 8;

p1a.LineWidth = LW;
p1b.LineWidth = LW;
%p2.LineWidth = LW;
p3.LineWidth = LW;
p4.LineWidth = LW;
p5.LineWidth = LW;

S1.YLabel.String = 'Elevation (m)';
%S2.YLabel.String = 'Discharge (m^3 s^{-1})';
S3.YLabel.String = {'Water velocity';'(m s^{-1})'};
S4.YLabel.String = 'Diameter (m)';
S5.YLabel.String = '\tau_b (Pa)';

S5.XLabel.String = 'Distance from water source (km)';

S1.FontSize = FS;
%S2.FontSize = FS;
S3.FontSize = FS;
S4.FontSize = FS;
S5.FontSize = FS;

% Making the axis right
S1.YLim = [0 800];
S5.YLim = [150 550];

TL = [0.015 0.03];
S1.TickLength = TL;
%S2.TickLength = TL;
S3.TickLength = TL;
S4.TickLength = TL;
S5.TickLength = TL;

S1.XMinorTick = 'on';
S1.YMinorTick = 'on';
%S2.XMinorTick = 'on';
%S2.YMinorTick = 'on';
S3.XMinorTick = 'on';
S3.YMinorTick = 'on';
S4.XMinorTick = 'on';
S4.YMinorTick = 'on';
S5.XMinorTick = 'on';
S5.YMinorTick = 'on';

% Arranging the position of the subplots
sub_w = 0.37;
S1.Position(3) = sub_w;
S3.Position(3) = sub_w;
S4.Position(3) = sub_w;
S5.Position(3) = sub_w;

x_left = 0.11;
S1.Position(1) = x_left;
S4.Position(1) = x_left;
x_right = 0.5;
S3.Position(1) = x_right;
S5.Position(1) = x_right;
y_bot = 0.14;
S4.Position(2) = y_bot;
S5.Position(2) = y_bot;

l1.Position = [0.2628 0.7804 0.2090 0.1134];

S5.XLabel.Position = [-0.8823 64 -1.0];

%Adding text on there
subplot(S1)
%xposfact = 0.05;
xposfact = 0.95;
xposfact2 = 0.96;
yposfact = 0.85;
%yposfact = 0.15;
S1_tx = S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact;
S1_ty = S1.YLim(2) - (S1.YLim(2)-S1.YLim(1)).*yposfact;
tA = text(S1_tx,S1_ty,'A');
%subplot(S2)
%S2_tx = S2.XLim(2) - (S2.XLim(2)-S2.XLim(1)).*xposfact;
%S2_ty = S2.YLim(2) - (S2.YLim(2)-S2.YLim(1)).*yposfact;
%tB = text(S2_tx,S2_ty,'B');
subplot(S3)
S3_tx = S3.XLim(2) - (S3.XLim(2)-S3.XLim(1)).*xposfact;
S3_ty = S3.YLim(2) - (S3.YLim(2)-S3.YLim(1)).*yposfact;
tC = text(S3_tx,S3_ty,'B');
subplot(S4)
S4_tx = S4.XLim(2) - (S4.XLim(2)-S4.XLim(1)).*xposfact;
S4_ty = S4.YLim(2) - (S4.YLim(2)-S4.YLim(1)).*yposfact;
tD = text(S4_tx,S4_ty,'C');
subplot(S5)
S5_tx = S5.XLim(2) - (S5.XLim(2)-S5.XLim(1)).*xposfact;
S5_ty = S5.YLim(2) - (S5.YLim(2)-S5.YLim(1)).*yposfact;
tE = text(S5_tx,S5_ty,'D');

tA.FontSize = FS;
%tB.FontSize = FS;
tC.FontSize = FS;
tD.FontSize = FS;
tE.FontSize = FS;
