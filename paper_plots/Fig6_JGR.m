% Script to compare the simulations from different synth season plots
%
%
% The usal, run the load script to get a set of simulations
% ST_ar = load_comp_runs();


dx = ST_ar(1).cst.dx;
M = ST_ar(1).cst.M;

x_stag = linspace(dx./2,dx.*(M-1)-dx./2,M-1).*1e-3;
x_plot = linspace(0,dx.*(M-1),M).*1e-3;

x_span = [M-5:M];
x_span2 = [M-5:M-1];

% from end to end-10 kinda something happening

%t_slice = [160*24:1:160*24*2];
% Just to plot 120 days out of the whole schmilblick
t_slice = [185*24:1:160*24*2-1-20*24];
%t_sl_zoom = [185*24 + 60*24:1:160*24*2-1-10*24];
t_plot = (t_slice-t_slice(1)) ./ 24;

%t_pl_end = 160*2 -10 - 185;
t_pl_end = 115;
t_start_cut = 60;

%xpos = [M M-1 M-3];
%xpos = [M-3 M-1 M];
xpos = [M-4 M];
%xpos_st = [M-1 M-3 M-5];
xpos_st = xpos -1;

sy = 4;
sx = 1;

LW = 1.75;
LW1 = 1.75;
LW2 = 1.25;
LW3 = 1;
MS = 9;
MS2 = 0.01;
FS = 10;
FSlg = 8;
FSax = 8;

plot_nb = length(ST_ar);

ln_nb = length(xpos);

show_uncoup = 0;

M_list = {'s','v','<','>','d','x'};
%col_list = [0 0.4470 0.7410; 0.8500 0.3250 0.0980; 0.9290 0.6940 0.1250];
%col_list = [0.9290 0.6940 0.1250; 0.8500 0.3250 0.0980; 0 0.4470 0.7410];
col_list = [0.9290 0.6940 0.1250; 0 0.4470 0.7410];

%Leg_txtA = {'@ 29.1 km','@ 29.7 km','@ 30 km','@ 29.1 km decoupled','@ 29.7 km decoupled','@ 30 km decoupled'};
Leg_txtA = {'@ 29.6 km','@ 30 km','@ 29.6 km decoupled','@ 30 km decoupled'};

f1 = figure;
f1.Units = 'centimeters';
f1.Position(1) = 2;
f1.Position(3) = 14;
f1.Position(4) = 16;

        ii = 1;
        jj = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    si = 1;
    S2 = subplot(sy,sx,si);

        %yyaxis left
        for jj = 1:ln_nb
            p21(jj) = plot(t_plot,ST_ar(ii).Sch(t_slice,xpos(jj)),'Linewidth',LW2);
            hold on
            p21(jj).LineStyle = '--';
            p21(jj).Color = col_list(jj,:);
            p22(jj) = plot(t_plot,ST_ar(ii+1).Sch(t_slice,xpos(jj)),'Linewidth',LW1);
            p22(jj).Color = col_list(jj,:);
        end
        S2.YLabel.String = 'S_{ch} (m^2)';
        S2.XLabel.String = 'Days during simulation';

		S2.YLim = [20 80];
        grid on

        % MAKING THE SHADED AREA
		shade_y = [70 70];
		shade_x = [t_start_cut t_pl_end];
		ar_sh = area(shade_x,shade_y);
        ar_sh.BaseValue = 25;
        ar_sh.BaseLine.Color = 'none';
		ar_sh.FaceColor = 'none';
		ar_sh.EdgeColor = 'k';
		%ar_sh.FaceAlpha = 0.35;
        
        % Adding explanatory text
        txtp2 = text(87.6,65.5,'Panels B, C and D');
        txtp2.FontSize = FSlg;

        yyaxis right
        p23(1) = plot(t_plot,ST_ar(ii).Qch(t_slice,xpos(end)),'Linewidth',LW2);
        p23(1).Color = [1 1 1].*0.65;
        S2.YAxis(2).Color = [1 1 1].*0.4; % To just tackle the right axis
        S2.YLim = [0 215];
        S2.YLabel.String = 'Q_{ch}(x=X_L) (m^3/s)';

        yyaxis left
        axS2a = gca;
        axS2b = axes('position',get(gca,'position'), 'visible','off');
        l1a = legend(axS2a,p22,Leg_txtA(1:ln_nb));
        l1a.Location = 'West';
        if show_uncoup == 1
            l2a = legend(axS2b,p21,Leg_txtA(ln_nb+1:ln_nb*2));
        end
        

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    si = si+1;
    S1 = subplot(sy,sx,si);
        
        for jj = 1:ln_nb
            p12(jj) = plot(t_plot,ST_ar(ii+1).V_a(t_slice,xpos(jj)),'Linewidth',LW1);
            hold on
            %p12(jj).LineStyle = '--';
            p12(jj).Color = col_list(jj,:);
        end
        %S1.YLabel.String = 'V_{s} / D_i (m)';
        S1.YLabel.String = 'V_{s} (m^2)';
		S1.YLim = [0 8];
        
        grid on

        if show_uncoup == 1;
            %yyaxis right
            for jj=1:ln_nb
                p11(jj) = plot(t_plot,ST_ar(ii).V_a(t_slice,xpos(jj)),'Linewidth',LW2);
                hold on
                p11(jj).LineStyle = '--';
                p11(jj).Color = col_list(jj,:);
            end
            %S1.YAxis(2).Color = [1 1 1].*0.4; % To just tackle the right axis
            S1.YLim = [0 70];
            %S1.YLabel.String = 'Decoupled V_{s} / D_i (m)';
            S1.YLabel.String = 'Decoupled V_{s} (m^2)';
        end

        %clegA = columnlegend(2,Leg_txtA,'Location','NorthWest','FontSize',FSlg,'boxoff');
        if show_uncoup == 0
            legA = legend(Leg_txtA(1:2));
        else
            legA = legend(Leg_txtA);
        end

    si = si+1;
    S3 = subplot(sy,sx,si);
    
        for jj = ln_nb:-1:1
            p31(jj) = plot(t_plot,abs(ST_ar(ii).dphicdx(t_slice,xpos_st(jj))),'Linewidth',LW2);
            hold on
            p31(jj).LineStyle = '--';
            p31(jj).Color = col_list(jj,:);
            p32(jj) = plot(t_plot,abs(ST_ar(ii+1).dphicdx(t_slice,xpos_st(jj))),'Linewidth',LW1);
            p32(jj).Color = col_list(jj,:);
        end
        %Just to make a mess
        p31b = plot(t_plot,abs(ST_ar(ii).dphicdx(t_slice,xpos_st(end))),'Linewidth',LW3);
        p31b.Color = 'k';
        p31b.LineStyle = '--';
        
        if show_uncoup == 1
            l3b = legend([p31b],'@ 30 km decoupled');
        end

        S3.YLabel.String = '|d\phi_{ch}/dx| (Pa/m)';

        grid on

    si = si+1;
    S4 = subplot(sy,sx,si);

        for jj = ln_nb:-1:1
            p41(jj) = plot(t_plot,ST_ar(ii).q_ttc(t_slice,xpos(jj)),'Linewidth',LW2);
            hold on
            p41(jj).LineStyle = '--';
            p41(jj).Color = col_list(jj,:);
            % to plot q_tt / q_ttc
            q_ratio = ST_ar(ii+1).q_tt(t_slice,xpos(jj)) ./ ST_ar(ii+1).q_ttc(t_slice,xpos(jj));
            p42(jj) = plot(t_plot,q_ratio,'Linewidth',LW1);
            %p42(jj) = plot(t_plot,ST_ar(ii+1).q_ttc(t_slice,xpos(jj)),'Linewidth',LW1);
            p42(jj).Color = col_list(jj,:);
        end
        %Just to make a mess
        p41b = plot(t_plot,ST_ar(ii).q_ttc(t_slice,xpos_st(end)),'Linewidth',LW3);
        p41b.Color = 'k';
        p41b.LineStyle = '--';
        
        if show_uncoup == 1
            l4b = legend([p41b],'@ 30 km decoupled');
        end

        S4.YLabel.String = 'r_{V}';
        S4.XLabel.String = 'Days during simulation';



%%%%%%%%%%
% Making shit neat again

%Just to make a bit of a mess...

for jj = 1:ln_nb
    p21(jj).Visible = 'off';
    p31(jj).Visible = 'off';
    p41(jj).Visible = 'off';
end
if show_uncoup == 0;
    p31b.Visible = 'off';
    p41b.Visible = 'off';
end

%p31b.Visible = 'off';
%p41b.Visible = 'off';

%l3b.Visible = 'off';
%l4b.Visible = 'off';

S1.FontSize = FSax;
S2.FontSize = FSax;
S3.FontSize = FSax;
S4.FontSize = FSax;

Xax_span1 = [0 t_pl_end];
S2.XLim = Xax_span1;
Xax_span = [t_start_cut t_pl_end];
S1.XLim = Xax_span;
S3.XLim = Xax_span;
S4.XLim = Xax_span;

% Making axes neat
S4.YLim = [0 1];

% Re-adjusting boxes for subplots
sub_w1 = 0.8;
S2.Position(3) = sub_w1;
sub_w2 = 0.75;
S1.Position(3) = sub_w2;
S3.Position(3) = sub_w2;
S4.Position(3) = sub_w2;

sub_h = 0.195;
S1.Position(4) = sub_h;
S2.Position(4) = sub_h;
S3.Position(4) = sub_h;
S4.Position(4) = sub_h;

S2.Position(1:2) = [0.1 0.78];

S1.Position(2) = 0.522;
S3.Position(2) = 0.292;
S4.Position(2) = 0.062;

%legA.Position(1) = 0.242;
%legA.Position(2) = 0.6459;
legA.Box = 'off';
legA.Visible = 'off';

if show_uncoup == 1
    l3b.Position(1) = 0.2941;
    l3b.Box = 'off';
    l4b.Box = 'off';
    l3b.FontSize = FSlg;
    l4b.FontSize = FSlg;
end

%l1a.Position(1) = 0.3576;
%l1a.Position(2) = 0.8374;
l1a.Box = 'off';
if show_uncoup == 1
    l2a.Position(1) = 0.62;
    l2a.Position(2) = 0.93;
    l2a.Box = 'off';
    l2a.FontSize = FSlg;
end

l1a.FontSize = FSlg;
legA.FontSize = FSlg;

% Ticks
S1.XMinorTick = 'on';
S1.XGrid = 'on';
if show_uncoup == 1
    S1.YMinorTick = 'on';
    subplot(S1)
    yyaxis left
end
S1.YMinorTick = 'on';
S1.YGrid = 'on';

S2.XMinorTick = 'on';
S2.XGrid = 'on';
S2.YMinorTick = 'on';
subplot(S2)
yyaxis left
S2.YMinorTick = 'on';
S2.YGrid = 'on';

S3.XMinorTick = 'on';
S3.YMinorTick = 'on';
S3.XGrid = 'on';
S3.YGrid = 'on';

S4.XMinorTick = 'on';
S4.YMinorTick = 'on';
S4.XGrid = 'on';
S4.YGrid = 'on';

%Adding text on there
subplot(S2)
yyaxis left
xposfact = 0.97;
yposfact2 = 0.15;
tA = text(S2.XLim(2) - (S2.XLim(2)-S2.XLim(1)).*xposfact,S2.YLim(2) - (S2.YLim(2)-S2.YLim(1)).*yposfact2,'A');
subplot(S1)
tB = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact,S1.YLim(2) - (S1.YLim(2)-S1.YLim(1)).*yposfact2,'B');
subplot(S3)
tC = text(S3.XLim(2) - (S3.XLim(2)-S3.XLim(1)).*xposfact,S3.YLim(2) - (S3.YLim(2)-S3.YLim(1)).*yposfact2,'C');
subplot(S4)
tD = text(S4.XLim(2) - (S4.XLim(2)-S4.XLim(1)).*xposfact,S4.YLim(2) - (S4.YLim(2)-S4.YLim(1)).*yposfact2,'D');

tA.FontSize = FS;
tB.FontSize = FS;
tC.FontSize = FS;
tD.FontSize = FS;
