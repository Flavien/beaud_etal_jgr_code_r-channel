%Script to recalculate some of the interesting quantities for interpreting
%the results of your simulations ran with the pdepe_ta_mere.m file
%
%In the pdepe_channel_esker.m file shit is done inside that first script.
%
%

%Recomputing the model_var.ables used in the solving routine


%And let's start with the computing related to hydrology
%%%%%%%%%
%%  HYDRO
%%%%%%%%%
S_sol_avg = (store_var.Sch(:,2:end) + store_var.Sch(:,1:end-1)) ./ 2;
Q_c = S_sol_avg.*0;
%Q_c = store_var.Sch.*0;

Va_avg = (store_var.V_a(:,2:end) + store_var.V_a(:,1:end-1)) ./ 2;

R_i = sqrt((2.*store_var.Sch)./pi);
D_i = R_i.*2;
R_i_avg = sqrt((2.*S_sol_avg)./pi);

phi_br_mat = repmat(model_var.phi_br,cst.tstp_nb,1);
p_ice_mat = repmat(model_var.p_ice_init,cst.tstp_nb,1);

phi_a_mat = cst.g .* cst.rho_w .* store_var.V_a ./ (1-cst.lambda_s) ./ D_i;

if sim_cond.bed_feedback == 1
    phi_c = store_var.pc + phi_br_mat + phi_a_mat;
elseif sim_cond.bed_feedback == 0
    disp('no feedback with the bed')
    phi_c = store_var.pc + phi_br_mat;
end

dphicdx = (phi_c(:,2:end) - phi_c(:,1:end-1)) ./ cst.dx;
dpcdx = (store_var.pc(:,2:end) - store_var.pc(:,1:end-1)) ./ cst.dx;
    dphicdx_avg(:,2:cst.M-1) = (dphicdx(:,2:end) + dphicdx(:,1:end-1)) ./ 2;
    dphicdx_avg(:,1) = dphicdx(:,1);
    dpcdx_avg(:,2:cst.M-1) = (dpcdx(:,2:end) + dpcdx(:,1:end-1)) ./ 2;
    dpcdx_avg(:,1) = dpcdx(:,1);

    pc_avg = (store_var.pc(:,2:end) + store_var.pc(:,1:end-1)) ./ 2;

%Ice and bed perimeters
P_i(:,2:cst.M) = pi .* R_i_avg;
P_b(:,2:cst.M) = 2*R_i_avg; %For now as a simple assumption. Change when bedrock erosion included.
P_i(:,1) = pi .* R_i(:,1);
P_b(:,1) = 2*R_i(:,1); %For now as a simple assumption. Change when bedrock erosion included.

%Wetted perimeter for fullid channels
P_w = P_i + P_b;

%Hydraulic perimeter
R_H_avg(:,2:cst.M) = S_sol_avg ./ P_w(:,2:cst.M);
%R_H = S_sol_avg ./ P_w;
R_H_avg(:,1) = store_var.Sch(:,1) ./ P_w(:,1);


%Averaged friction coefficient
f_i = cst.f_i_p1 ./ (R_H_avg.*(1/3));
f_b = cst.f_b_p1 ./ (R_H_avg.*(1/3));
f_R_avg = (f_i .*P_i + f_b .* P_b) ./ P_w;

%Q_c = -cst.kc .* S_sol_avg.^cst.alpha_c .* abs(dphicdx).^(cst.beta_c -2) .* dphicdx;
Qc1 = (8.*S_sol_avg.^3) ./ (P_w(:,2:cst.M).*cst.rho_w.*f_R_avg(:,2:cst.M));
Qc2 = dphicdx .* abs(dphicdx).^(-1/2);
Q_c(:,2:cst.M) = - Qc1.^(1/2) .* Qc2;
%Q_c = - Qc1.^(1/2) .* Qc2;

disp('Assuming Q_c is constant to recompute')
Q_c(:,1) = Q_c(:,2);

Qc_avg = (Q_c(:,1:end-1) + Q_c(:,2:end)) ./ 2;
Qc_avg(:,1) = Q_c(:,1);


%SOMETHING ABOUT THE QW IS FISHY
[v_w] = water_vel_smooth(Q_c,store_var.Sch,S_sol_avg);
%v_w = Q_c ./ S_sol_avg;
%v_w = Q_c ./ store_var.Sch;

N_ch = p_ice_mat - store_var.pc;

vc_c_node = cst.A_tild .* store_var.Sch .* abs(N_ch).^(cst.n-1) .* N_ch;
vc_c = (vc_c_node(:,2:end) + vc_c_node(:,1:end-1))./2;

q_w = 0;
Xi = abs(Q_c(:,2:cst.M).*dphicdx) + abs(cst.lc.*q_w.*dphicdx);
%Xi = abs(Q_c.*dphicdx) + abs(cst.lc.*q_w.*dphicdx);

f_c = 1;
Pi_c = -cst.c_t.*cst.c_w.*cst.rho_w .* (Q_c(:,2:cst.M) + f_c.*cst.lc.*q_w) .* dpcdx;
%Pi_c = -cst.c_t.*cst.c_w.*cst.rho_w .* (Q_c + f_c.*cst.lc.*q_w) .* dpcdx;

vo_c = (Xi - Pi_c)./(cst.rho_i * cst.L);

%And now moving on to the part related to sediment transport.
%%%%%%%%%%%%%%%%%%
%% SEDIMENT STUFFS
%%%%%%%%%%%%%%%%%%

%For the tests where a sedimentation rate was imposed.
    %vol_sed_rate = - 0.05 / (3600.*24).*2.*R_i_avg;
    %vol_sed_rate = zeros(cst.tstp_nb,cst.M-1);
    %vol_sed_rate(:,50:end) = vol_sed_rate(:,50:end) - 0.05 / (3600.*24);



%Computing the shear stress
%First without the friction param to save a computation
shear_part = (1/8) .* cst.rho_w .* v_w .* abs(v_w);
%Total shear stress
tau_tot = shear_part .* f_R_avg;
%Bed shear stress per unit width
tau_b = shear_part .* f_b;
tau_i = shear_part .* f_i;

% Recalculating shear velocity on the bed
u_shear_b = sqrt(tau_b./cst.rho_w);
% Recalculating shear velocity on the ice
u_shear_i = sqrt(tau_i./cst.rho_w);
% Recalculating shear velocity averaged
u_shear_tot = sqrt(tau_tot./cst.rho_w);

%NOW THE SEDIMENT / EROSION PART OF THINGS
%Shields stress
tau_s = tau_b ./ ((cst.rho_s-cst.rho_w) .* cst.g .* model_var.D );

%Transport capacity (Fernandez-Luque and van Beek, 1976)
tau_diff = tau_s - cst.tau_c;
tau_diff(tau_diff < 0) = 0;
q_tc = 5.7 .* (cst.r .* cst.g .* model_var.D.^3).^(1/2) .* (tau_diff).^(3/2);


V_a_stag = (store_var.V_a(:,2:end) + store_var.V_a(:,1:end-1))./2;

%Sediment velocity
tau_frac = tau_s ./ cst.tau_c;
tau_frac(tau_frac < 1) = 1;
u_s = 1.56 .* (tau_frac -1).^0.56 .* sqrt(cst.r.*cst.g.*model_var.D);
u_s(u_s > v_w) = v_w(u_s > v_w);

% Staggering of q_tc ot mimic actual solving
q_tc_stag = zeros(cst.tstp_nb,cst.M);
q_tc_stag(:,1:cst.M-1) = (q_tc(:,2:end) + q_tc(:,1:end-1)) ./ 2;

q_tc_stag(:,cst.M) = q_tc_stag(:,cst.M-1) + q_tc(:,cst.M) - q_tc(:,cst.M-1);

u_s_stag = zeros(cst.tstp_nb,cst.M);
u_s_stag(:,1:cst.M-1) = (u_s(:,2:end) + u_s(:,1:end-1)) ./ 2;

u_s_stag(:,cst.M) = u_s_stag(:,cst.M-1) + u_s(:,cst.M) - u_s(:,cst.M-1);

%Testing again Inoue's method to see if it changes with different timesteps
%V_bc = q_tc ./ u_s .* D_i;
V_bc = q_tc_stag ./ u_s_stag .* D_i;
%V_ratio = V_a_stag ./ V_bc;
V_ratio = store_var.V_a ./ V_bc;
V_ratio(V_ratio >= 1) = 1;
V_ratio(q_tc_stag <= 0) = 0;
q_b = V_ratio .* q_tc_stag;
q_tt = q_b.*D_i;

% defining it here so that it is consistent with q_tt
q_ttc = q_tc_stag.*D_i;

% Calculating some qtities that are proxy for the seismic power

% for water flow: Pw \propto u_*^(14/3) and for bedload Pb \propto q_b D^3
Pwi_prox = u_shear_i.^(14/3);
Pwb_prox = u_shear_b.^(14/3) .* P_b;
Pw_tot_prox = u_shear_tot.^(14/3) .* P_w;

Pb_prox = q_tt.*(model_var.D).^3;

% making a little structure with seismic stuffs to avoid mess
seismic_proxies = struct('u_shear_i',u_shear_i,...
                        'u_shear_b',u_shear_b,...
                        'u_shear_tot',u_shear_tot,...
                        'tau_i',tau_i,...
                        'Pwi_prox',Pwi_prox,...
                        'Pwb_prox',Pwb_prox,...
                        'Pw_tot_prox',Pw_tot_prox,...
                        'Pb_prox',Pb_prox);



% CHANGE
% ADD the term for the source of sediment at some point
%q_s_eta = V_a_stag .* cst.dx ./ cst.dt;
q_s_eta = store_var.V_a .* cst.dx ./ cst.dt;

%Fraction covered for the sediment continuity following Inoue et al. (2014).
%Fc = 1; %Omitting the maco-roughness of the bed for now...
Fc = min(q_s_eta ./ q_tc,1);
Fc(q_tc <= 0) = cst.Fc_lim;
Fc(V_a_stag < 1e-3./cst.secondsinyear) = cst.Fc_lim;

q_tc_eta = Fc .* q_tc;

dqtc_va_dx = (q_tc_eta(:,2:end) - q_tc_eta(:,1:end-1))./ cst.dx;

dqtcdx = (q_tc(:,2:end) - q_tc(:,1:end-1))./ cst.dx;
dFcdx = (Fc(:,2:end) - Fc(:,1:end-1)) ./ cst.dx;

dVa_dt = (store_var.V_a(2:end,:) - store_var.V_a(1:end-1,:)) ./ cst.dt;

v_s = dVa_dt.*(1/(1-cst.lambda_s));

dS_dt = (store_var.Sch(2:end,:) - store_var.Sch(1:end-1,:)) ./ cst.dt;

% Making a structure to extract paarameters related to channel evolution
channel_evol = struct('vc_c',vc_c,...
                    'vo_c',vo_c,...
                    'Xi',Xi,...
                    'Pi_c',Pi_c,...
                    'N_ch',N_ch,...
                    'v_s',v_s);


%Computing the flux of sediment that should come out of the glacier terminus
%out_flux = q_b(:,end) .* D_i(:,end);
out_flux = q_tt(:,end);

field_test = isfield(sim_cond,'flux_per_uw');
if field_test == 0
    sim_cond.flux_per_uw = 0;
end

if sim_cond.flux_per_uw == 0
    in_mass_flux = model_var.sed_flux .* cst.rho_s;
elseif sim_cond.flux_per_uw == 1
    in_mass_flux = model_var.sed_flux .* D_i(:,1) .* cst.rho_s;
end
out_mass_flux = out_flux .* cst.rho_s;
out_mass_conc = out_mass_flux ./ Q_c(:,end);
