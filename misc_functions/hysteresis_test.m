%Little piece of code to sort out the direction of hysteresis between the
%discharge and the transport stage 
%
%

function [hyst_clock] = hysteresis_test(cp_var1,cp_var2,x_loc,dt_fact)

day = 24.*dt_fact;
s_end_lg = 160*24*2.*dt_fact-day;
s_start = 160*24.*dt_fact+1;

%hyst_clock = zeros(1,s_end_lg) -1;

for i=s_start:day:s_end_lg
    if i == s_start
        ii = 1;
    end
    max_Qc(ii) = max(cp_var1(i:i+day,x_loc));
    max_tstage(ii) = max(cp_var2(i:i+day,x_loc));
    pos_Qc_t = find(cp_var1(i:i+day,x_loc) == max_Qc(ii));
    pos_tst_t = find(cp_var2(i:i+day,x_loc) == max_tstage(ii));
    pos_Qc(ii) = max(pos_Qc_t);
    pos_tst(ii) = max(pos_tst_t);

    if max_tstage == 0
        hyst_clock(ii) = -1;
    elseif pos_Qc(ii) == pos_tst(ii)
        hyst_clock(ii) = 0.5;
    elseif pos_Qc(ii) > pos_tst(ii)
        hyst_clock(ii) = 1;
    elseif pos_Qc(ii) < pos_tst(ii)
        hyst_clock(ii) = 0;
    end

    ii = ii +1;
    %if i > 25240+1000
    %    keyboard;
    %end
end
