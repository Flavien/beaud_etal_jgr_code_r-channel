% File to load the run files that I want,
% send them in the prep function and spit an array of structures
% back so that I can feed these to the plot function.
%
%

function [comp_run_array] = load_comp_runs()


    list_dx_comp = {'up_q_b_WDG30_dt900s_dx600.mat',...
                    'up_q_b_WDG30_dt900s.mat',...
                    'up_q_b_WDG30_dt900s_dx150.mat',...
                    'up_q_b_WDG30_dt900s_dx100.mat',...
                    'up_q_b_WDG30_dt900s_dx50.mat',...
                    'up_q_b_WDG30_dt900s_dx25.mat',...
                    'up_q_b_WDG30_dt900s_dx10.mat'};

    list_SS_target_dx100 = {'Qch50_SS_WDG_dx100_D60_qb_cstH20_BF_rerun.mat',...
                            'Qch50_SS_WDG_dx100_D60_qb_cstH205_BF.mat',...
                            'Qch50_SS_WDG_dx100_D60_qb_cstHa_BF.mat',...
                            'Qch50_SS_WDG_dx100_D60_qb_cstH215_BF.mat',...
                            'Qch50_SS_WDG_dx100_D60_qb_cstHb_BF.mat',...
                            'Qch50_SS_WDG_dx100_D60_qb_cstH225_BF.mat'};

    list_synth_seas4 = {'WDG_SEAS4_dx100_D60_qbF_noBF.mat'...
                        'SIN_Qch_synth_WDG_SEAS4_dx100_D60_qbF_BF.mat'};

    list_esker_comp = {'SIN_Qch_synth_WDG_SEAS4_dx100_D60_qbF_BF.mat',...
                        'SIN_Qch_synth_PAR_SEAS4_dx100_D60_qbF_BF.mat',...
                        'SIN_Qch_synth_800_SEAS4_dx100_D60_qbF_BF.mat',...
                        'SIN_Qch_synth_600_SEAS4_dx100_D60_qbF_BF.mat',...
                        'WDG_SEAS4_dx100_D60_qbF_Q40_BF.mat',...
                        'WDG_SEAS4_dx100_D60_qbF_Q60_BF.mat',...
                        'WDG_SEAS4_dx100_D60_qbFb_BF.mat',...
                        'WDG_SEAS4_dx100_D60_qbFa_dt900_BF.mat'};


    list_freq = {'SIN_Qch50_1d_dx100_D60_qbJe_cst_BF.mat',...
                'SIN_Qch50_1d_dx100_D60_qbJe_cst_noBF.mat',...
                'SIN_Qch50_6d_dx100_D60_qbJe_cst_BF.mat',...
                'SIN_Qch50_6d_dx100_D60_qbJe_cst_noBF.mat',...
                'SIN_Qch50_1m_dx100_D60_qbJe_cst_BF.mat',...
                'SIN_Qch50_1m_dx100_D60_qbJe_cst_noBF.mat',...
                'SIN_Qch50_3m_dx100_D60_qbJe_cst_BF.mat',...
                'SIN_Qch50_3m_dx100_D60_qbJe_cst_noBF.mat'};
                
    %run_list = list_dx_comp;
    %run_list = list_SS_target_dx100;
    %run_list = list_synth_seas4;
    run_list = list_freq; %This is the one you need for individual plots!!
    %run_list = list_esker_comp;

    run_nb = length(run_list);

    disp('Hydro only for recomputation')
    eros_and_hydro = 0;

    if eros_and_hydro == 0
        for i = 1:run_nb
            message = strcat('load file ',num2str(i));
            disp(message)
            %file_to_load = run_list{i};
            % Modify path based on location of simulations
            path_to_file = 'Simulation_repo/freq_test/';
            %path_to_file = 'Simulation_repo/Season/';
            %path_to_file = 'Simulation_repo/';
            file_to_load = strcat(path_to_file,run_list{i});
            
            comp_run_struct = prep_comp_runs(file_to_load,'load_nothing');

            comp_run_array(i) = comp_run_struct;
        end
    elseif eros_and_hydro == 1
        str_loc = 1;
        for i = 1:2:run_nb
            message = strcat('load file ',num2str(i));
            disp(message)
            file_to_load = run_list{i};
            file_to_load2 = run_list{i+1};
            %file_to_load = strcat(path_to_file,run_list{i});
            
            comp_run_struct = prep_comp_runs(file_to_load,file_to_load2);

            comp_run_array(str_loc) = comp_run_struct;
            str_loc = str_loc + 1;
        end
    end
