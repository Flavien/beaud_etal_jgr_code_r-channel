% File to add the path to the directories where I store functions
% This will help to clean the main directory a bit
%
%
addpath('misc_functions/')
addpath('sub_functions/')
addpath('load_and_prep/')
addpath('paper_plots/')
