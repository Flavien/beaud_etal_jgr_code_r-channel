%This is the main script to run the simulations.
%
%first call to init_esker.m to initialize
%then start a time loop in which making_eskers.m is called

clear all
clc

global time_loop 

%Initializing the parameters
[model_var,cst,store_var,sim_cond] = init_esker();

t = cst.dt:cst.dt:cst.t_end;
%x_stag = (x(2:end) + x(1:end-1)) ./ 2;

if sim_cond.load_init_run == 1
    load IC_SS_Qch50_WDG_dx100.mat
        %load IC_SS_Qch50_init.mat
        %load IC_SS_Qch50_WDG_init.mat
    %load IC_SS_Qch50_WDG30_init.mat
    %load IC_SS_Qch50_WDG30_dx10_init.mat
    %load IC_SS_Qch50_WDG30_dx50_init.mat
    %load IC_SS_Qch50_WDG30_dx25_init.mat
    %load IC_SS_Qch50_WDG30_dx100_init.mat
    %load IC_SS_Qch50_WDG30_dx150_init.mat
    %load IC_SS_Qch50_WDG30_dx600_init.mat

    model_var.pc_init = pc_IC_init;
    model_var.S_init = Sch_IC_init;
    %model_var.S_init = Sch_IC_init - 12;
    %model_var.S_init = Sch_IC_init - 1 .*2.* sqrt(2.*Sch_IC_init./pi) ;
end

for time_loop=1:cst.tstp_nb

    if time_loop == 2000
        save matlab
    end

    [pc_new,Sch_new,V_a_new] = testing_eskers(time_loop,model_var,cst,store_var,sim_cond);

    store_var.pc(time_loop,:)   = pc_new;
    store_var.Sch(time_loop,:)  = Sch_new;
    store_var.V_a(time_loop,:)  = V_a_new;
    %keyboard
end

save
% A surface plot is often a good way to study a solution.
no_plot = 0;
if no_plot == 0

    figure(20)
    surf(cst.x,t,store_var.V_a);
    title('V_a(x,t)')
    xlabel('Distance x')
    ylabel('Time t')
    shading('interp')

    figure(21)
    surf(cst.x,t,store_var.Sch);
    title('S\_{ch}(x,t)')
    xlabel('Distance x')
    ylabel('Time t')
    shading('interp')

    figure(23)
    surf(cst.x,t,store_var.pc);
    title('p\_{ch}(x,t)')
    xlabel('Distance x')
    ylabel('Time t')
    shading('interp')

    what_you_need_in_ode_aftermath
    figure(22)
    surf(cst.x,t,Q_c);
    title('Q\_{ch}(x,t)')
    xlabel('Distance x')
    ylabel('Time t')
    shading('interp')
end
