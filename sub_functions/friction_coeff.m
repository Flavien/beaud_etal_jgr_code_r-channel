%Function to clean up ode solving
%
% calculating the friction coefficient
%
%

function [f_R,f_b] = friction_coeff(R_H,P_i,P_b,P_w,cst);

    %Averaged friction coefficient
    f_i = cst.f_i_p1 ./ (R_H.*(1/3));
    f_b = cst.f_b_p1 ./ (R_H.*(1/3));
    f_R = (f_i .*P_i + f_b .* P_b) ./ P_w;
