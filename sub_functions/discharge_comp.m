%Function to clean up ode solving
%
% calculating the discharge
%
%

function [Q_c,Qc_avg] = discharge_comp(Sch_avg,P_w,f_R,dphicdx,model_var,cst,Qupstr);

    Qc1 = (8.*Sch_avg.^3) ./ (P_w(2:cst.M).*cst.rho_w.*f_R(2:cst.M));
    Qc2 = dphicdx .* abs(dphicdx).^(-1/2);
    Q_c(2:cst.M) = - Qc1.^(1/2) .* Qc2;

    %Q_c(1) = model_var.flux_bc; %BC at the top of the domain, since it's on ta staggered grid.
    Q_c(1) = Qupstr; %BC at the top of the domain, since it's on ta staggered grid.
    Qc_avg = (Q_c(1:end-1) + Q_c(2:end)) ./ 2;
    %Qc_avg(1) = model_var.flux_bc;
    Qc_avg(1) = Qupstr;
