%Function to clean up ode solving
%
% calculating varius radii and perimeters
%
%

function [R_i,P_i,P_b,R_H,P_w] = radii_and_perimeters(S_i,Si_avg,Sch_avg,S_ch,cst);

    %Radius of channel in ice
    R_i(2:cst.M) = sqrt((2.*Si_avg)./pi);
    R_i(1) = sqrt((2.*S_i(1))./pi);
    %Ice and bed perimeters
    P_i(2:cst.M) = pi .* R_i(2:cst.M);
    P_b(2:cst.M) = 2*R_i(2:cst.M); %For now as a simple assumption. Change when bedrock erosion included.
    P_i(1) = pi .* R_i(1);
    P_b(1) = 2.*R_i(1);
    %I assume that the water flow will level sediment in the channel and that the bed perimeter will only 
    %change when bedrock erosion is accounted for. The P_b will become of function of the erosion.

    %Wetted perimeter in the case of filled conduits
    P_w = P_i + P_b;

    %Then from P_b I could calculate S_b
    %For now because of the levelling S_b = 0;

    %Hydraulic perimeter
    R_H(2:cst.M) = Sch_avg ./ (P_i(2:cst.M) + P_b(2:cst.M));
    R_H(1) = S_ch(1) ./ (P_i(1) + P_b(1));

    %When the conduit is filled:
    P_m = P_i;
