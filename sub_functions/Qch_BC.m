% Function to apply a factor to whatever water input I want so that it changes over time
% I'll start by applying it to the discharge input, and we'll see what to do from there
%
%

function [Qupstr] = Qch_BC(sim_cond,model_var,cst,T);

    if model_var.Q_fact(1) == 0
        Qupstr = model_var.flux_bc;
    else
        more_than_one_sin = 0;

        p_1day = 3600 * 24; % in seconds
        p_6days = p_1day * 6; % in seconds
        p_1month = p_1day * 30; % in seconds
        p_3months = p_1month * 3;
        
        if more_than_one_sin == 1
            sin_1day    = (sin((2*pi* T)./ p_1day - pi./2));
            sin_1day_o  = (sin((2*pi* T)./ p_1day - 2*pi./2)); % phase slightly offset, just because...
            sin_6days   = (sin((2*pi* T)./ p_6days - pi./2)); 
            sin_1month  = (sin((2*pi* T)./ p_1month - pi./2)); 
            sin_3months = (sin((2*pi* T)./ p_3months - pi./2)); 
        end
        
        % To apply a factor 1 <= sin_fact <= 2 of a given period
        %
        % If the period is 6 days
        sin_fact_6d = (sin((2*pi* T)./ p_6days - pi./2)) .* 1/2 + 1.5;
        % If the period is 1 days
        sin_fact_1d = (sin((2*pi* T)./ p_1day - pi./2)) .* 1/2 + 1.5;
        % If the period is 1 month
        sin_fact_1month = (sin((2*pi* T)./ p_1month - pi./2)) .* 1/2 + 1.5;
        % If the period is 3 month
        sin_fact_3months = (sin((2*pi* T)./ p_3months - pi./2)) .* 1/2 + 1.5;
        %To make a synthetic seasonal output with 4 diff sinusoids
            %sin_fact_full_seas = (sin_3months + sin_1month + sin_6days + sin_1day_o)./4 + 1.5;
        %Super simple season with a 3 months and 1 day sinusoid
            %sin_fact_simp_seas = (sin_3months + sin_1day_o)./2 + 1.5;

        %sin_fact = sin_fact_6d;
        %sin_fact = sin_fact_1d;
        sin_fact = sin_fact_1month;
        %sin_fact = sin_fact_3months;
        
        %sin_fact = 1;
        making_seas = 1;

        if making_seas == 0
            % FOR SIMPLE OSCILLATIONS
            Qupstr = model_var.flux_bc .* sin_fact;
        elseif making_seas == 1
            % FOR SYNTH SEASONS 
            % If no oscillations needed :
                dt_fact = 3600 ./ cst.dt;
                loc_Q = round(T./cst.dt); 
                
                %loc_Q = mod(loc_Q,160*24*dt_fact) + 1; % THIS IS TO REPEAT A GIVEN SEASON
                loc_Q = mod(loc_Q,160*24*dt_fact .* 2) + 1; % THIS IS TO HAVE TWO SEASONS RUN IN ONE GO
                Qupstr = model_var.flux_bc .* model_var.Q_fact(loc_Q);
        end
    end


