%Function to clean up ode solving
%
% calculating the smoothed water velocity otherwise it's a mess
%
%

function [v_w] = water_vel_smooth(Q_c,S_ch,Sch_avg);

    % Now calculating water flow speed
    %v_w = Q_c ./ S_ch;

    v_w = Q_c.*0;

    v_w(:,1) = Q_c(:,1) ./ S_ch(:,1);
    v_w(:,2:end) = Q_c(:,2:end) ./ Sch_avg;

    %Cheating a bit by smoothing the velocity, but it's quite necessary.
    v_w_avg = (v_w(:,2:end) + v_w(:,1:end-1)) ./ 2;
    v_w_avg2 = (v_w_avg(:,2:end) + v_w_avg(:,1:end-1)) ./ 2;
    v_w_sm = v_w;
    v_w_sm(:,2:end-1) = v_w_avg2;
    v_w = v_w_sm;
