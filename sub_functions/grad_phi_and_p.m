%Function to clean up ode solving
%
% calculating gradients in phi and pressure
%
%

function [dphicdx,dphicdx_avg,dpcdx,dpcdx_avg,pc_avg] = grad_phi_and_p(phi_c,pc_sol,cst)

    dphicdx = (phi_c(:,2:end) - phi_c(:,1:end-1))./cst.dx;

    dphicdx_avg(:,2:cst.M-1) = (dphicdx(:,2:end) + dphicdx(:,1:end-1)) ./ 2;
    dphicdx_avg(:,1) = dphicdx(:,1);

    dpcdx = (pc_sol(:,2:end) - pc_sol(:,1:end-1))./cst.dx;

    dpcdx_avg(:,2:cst.M-1) = (dpcdx(:,2:end) + dpcdx(:,1:end-1)) ./ 2;
    dpcdx_avg(:,1) = dpcdx(:,1);
    
    pc_avg = (pc_sol(:,2:end) + pc_sol(:,1:end-1)) ./ 2;
