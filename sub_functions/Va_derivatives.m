%Function to clean up ode solving
%
% calculating the spatial and time derivatives in sediment volume
%
%

function [dVa_dt] = Va_derivatives(q_tc_va,s3,cst,model_var,sim_cond,q_tc,D_i,D_i_stag)

    %Calculating the spatial derivative on sediment-flux-related terms
dqt_vadx = (q_tc_va(2:end) - q_tc_va(1:end-1)) ./ cst.dx;
    dqt_vadx_avg = (dqt_vadx(2:end) + dqt_vadx(1:end-1))./2;

    q_tc_va_avg = q_tc_va;
    % Probably better with a gradient if I don't want it to be too dependant on grid size.

    dqt_vadx_stag = zeros(1,cst.M);
    dqt_vadx_stag(2:cst.M) = (q_tc_va_avg(2:end) - q_tc_va_avg(1:end-1)) ./ cst.dx;
    if sim_cond.flux_per_uw == 0
        dqt_vadx_stag(1) = (q_tc_va_avg(1) - model_var.sed_flux) ./ cst.dx;
    elseif sim_cond.flux_per_uw == 1
        dqt_vadx_stag(1) = (q_tc_va_avg(1) - model_var.sed_flux.*D_i(1)) ./ cst.dx;
    end


    % NOTE I have to use the Fc(2:cst.M) here because I use Fc for dXXdx computation. If I use Fc_avg, I introduce
    % a slight difference in the factor used (Fc vs Fc_avg) which messes up sediment conservation
%dVa_dt(2:cst.M) = -1 .* (dqt_vadx - D_i(2:cst.M).*s3(2:cst.M)./cst.dx);
    dVa_dt(1:cst.M) = -1 .* (dqt_vadx_stag - D_i_stag(1:cst.M).*s3(1:cst.M)./cst.dx);

