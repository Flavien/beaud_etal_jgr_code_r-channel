% This script may be turned into a function later.
% It's primary goal is to produce a timeseries of water input that
% would hopefully somewhat mimick realistic meltseasons.
%
% I will use different syntheric curves added to each other to create a realistic timeseries
%

function [Q_fact] = water_input_shape(dt)
    
    dt_fact = 24*dt;

        %t_array = [1:160.*dt_fact]; % this is hours
    t_array = [1:160.*dt_fact]; % this is hours

    tanh_p = 15*dt_fact; % This is hours too
    tanh_pd = 25*dt_fact; % This is hours too

    %First I will create the background with two tanh curves
    lc_rise = 55*dt_fact;
    lc_down = 102*dt_fact;
    amp_fact_th = 2;
    seas_rise = tanh( (t_array.*pi)./tanh_p - (lc_rise.*pi)./tanh_p ) + 1;
    seas_down = - tanh( (t_array.*pi)./tanh_pd - (lc_down.*pi)./tanh_pd ) - 1;

    seas_back =  ((seas_rise + seas_down)./2) .* amp_fact_th + 1;

    % Now making just a two-weeks long spike in discharge
    sin_p2w = 23*dt_fact;
    sin_2w_lc = 58*dt_fact;
    t_2w = [1:sin_p2w];
    sin_2weeks = sin( (2.*pi.*t_2w)./sin_p2w - pi/2 )./2 + 0.5; 

    % Now making a time series of daily oscillations
    sin_p1d = dt_fact;
    sin_1d = sin( (2.*pi.*t_array)./sin_p1d - pi/2 )./5; 
    lc_1d = t_array(65*dt_fact:160*dt_fact);

    % Now making a time series of 6days oscillations
    sin_p6d = 6*dt_fact;
    sin_6d = sin( (2.*pi.*t_array)./sin_p6d - pi/2 )./4; 
    lc_6d = t_array(55*dt_fact:160*dt_fact);

    % Now making a time series of 10days oscillations
    sin_p10d = 10*dt_fact;
    sin_10d = sin( (2.*pi.*t_array)./sin_p10d - pi/2 )./6; 
    lc_10d = t_array(81*dt_fact:160*dt_fact);

    lc_10d_B = t_array(28*dt_fact:38*dt_fact);
    lc_10d_C = t_array(42*dt_fact:52*dt_fact);

    %% Adding things together
    seas_temp = seas_back;
    % Starting with the 2weeks peak
    seas_temp(sin_2w_lc:sin_2w_lc+sin_p2w-1) = seas_temp(sin_2w_lc:sin_2w_lc+sin_p2w-1) + sin_2weeks;

    %Now adding daily fluctuations in places
    seas_temp(lc_1d) = seas_temp(lc_1d) + sin_1d(1:length(lc_1d));

    %Now adding the week-long oscillations
    %seas_temp(lc_6d) = seas_temp(lc_6d) + sin_6d(1:length(lc_6d));

    %Now adding the 10d oscillations in the second part of timing
    seas_temp(lc_10d) = seas_temp(lc_10d) + sin_10d(1:length(lc_10d));

    %Now adding the 10d oscillations in the first part of timing
    seas_temp(lc_10d_B) = seas_temp(lc_10d_B) + (sin_10d(1:length(lc_10d_B)) + 0.34./2);
    seas_temp(lc_10d_C) = seas_temp(lc_10d_C) + (sin_10d(1:length(lc_10d_C)) + 0.34./2);

    seas_temp2 = seas_temp;

    % Making it dip a little bit on either side, just because...
    %keyboard
    %tilt_lines1 = linspace(0.7,1,30*dt_fact);
    %tilt_lines2 = linspace(1,0.7,40*dt_fact);
    tilt_lines2 = linspace(1,0.5,40*dt_fact); % Trying with a steeper tilt
    %    disp('making the season tilt on either ends')
    %    disp('making the season tilt at the end')
    %seas_temp(1:30*dt_fact) = seas_temp(1:30*dt_fact) .* tilt_lines1;
    %seas_temp(120*dt_fact+1:end) = seas_temp(120*dt_fact+1:end) .* tilt_lines2;

    % Making a forcing for 2 seasons that only decreases close to the end and not in the middle
    seas_temp2(120*dt_fact+1:end) = seas_temp2(120*dt_fact+1:end) .* tilt_lines2;
    
    %Q_fact = seas_temp;
    Q_fact = [seas_temp,seas_temp2];

    %close all
    plot_that = 1;

    if plot_that == 0
        figure
        plot(t_array(1:dt:end)./dt_fact,seas_back(1:dt:end),'x-')
        hold on
        plot(t_array(1:dt:end)./dt_fact,seas_temp(1:dt:end),'-')

        %Just making it easy to see different months
        month_st = 30;
        plot(month_st,[1:0.1:5]','x-')
        plot(month_st+30,[1:0.1:5]','x-')
        plot(month_st+30*2,[1:0.1:5]','x-')
        plot(month_st+30*3,[1:0.1:5]','x-')
        plot(30,[1:5]','x-')
    end

%keyboard
