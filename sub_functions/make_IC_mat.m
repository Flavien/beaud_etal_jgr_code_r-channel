% Just a quick routine to make a .mat file out of the initialization simulation
% 
% Yep not really anything else to add.
%

pc_IC_init = store_var.pc(end,:);
Sch_IC_init = store_var.Sch(end,:);

disp('check the name before moving on!')
pause

save('IC_SS_Qch50_WDG30_dx10_init.mat','pc_IC_init','Sch_IC_init')
