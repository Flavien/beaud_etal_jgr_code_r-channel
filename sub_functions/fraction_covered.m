%Function to clean up ode solving
%
% calculating the fraction covered
%
%

function [Fc,Fc_avg] = fraction_covered(q_s_eta,q_tc,cst);

    %Fraction covered for the sediment continuity following Inoue et al. (2014).
    % Although I'm not actually using Inoue's formulation for the very value
    Fc = min(q_s_eta ./ q_tc,1);
    Fc(q_tc <= 0) = cst.Fc_lim;

    %Fc(V_a < 1e-3./cst.secondsinyear) = cst.Fc_lim;
    Fc(q_s_eta < 1e-3./cst.secondsinyear.*cst.dt./cst.dx) = cst.Fc_lim;

    Fc_avg = (Fc(2:end) + Fc(1:end-1)) ./ 2;
