%Function to clean up ode solving
%
% calculating total and basal shear stresses
%
%

function [tau_tot,tau_b] = shear_stress(v_w,f_R,f_b,cst);

    %Computing the shear stress
    %First without the friction param to save a computation
    shear_part = (1/8) .* cst.rho_w .* v_w .* abs(v_w);
    %Total shear stress
    tau_tot = shear_part .* f_R;
    %Bed shear stress per unit width
    tau_b = shear_part .* f_b;
