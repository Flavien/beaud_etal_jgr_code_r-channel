%File to be run as an init of for the esker simulations
%This is gonna be a mix of the init files for the hydro and erosion simulations
%
%

function [model_var,cst,store_var,sim_cond] = init_esker()

%This is gonna be ugly, but hey...
%global store_q_t store_q_s store_q_tc store_tau_s store_v_w store_q_tt store_qtc_eta store_dphicdx store_s3
%global store_detadx store_eta
%global time_loop store_dvadt store_Fc
global time_loop

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   SIMULATION CONTIDIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load_init_run = 0;

%%%%%%%%%%% WATER %%%%%%%%%%%
upstream_Qc_flux = 1;

distributed_bc_input = 0;

distributed_bs_input = 0;

%%%%%%%%%%% SEDIMENT %%%%%%%%%%%
upstream_qs_flux = 1;

flux_per_uw = 0;

downstream_qs_flux = 0;

distributed_Vs_input = 0;

distributed_source = 0;

%%%%%%%%%%% HYDRO/SED FEEDBACK %%%%%%%%%%%
bed_feedback = 1;


%Defining the grid
%Number of nodes
%M = 101;
M = 301;
%M = 51;
%grid size
%dx = 500; %(m)
%dx = 300; %(m)
dx = 100; %(m)
%dx = 250; %(m)
% To test the flume
%dx = 0.2;
%time step
dt = 3600;
%dt = 3600*2;
%dt = 900;
%dt = 100;
%End of simulation time
season_long = 24*30*3;
month_long = 24*30;
fake_seas = 160*24;
%tstp_nb = 100;
%tstp_nb = 40;
%tstp_nb = (season_long + month_long*2).*4;
%% THIS IS WHAT IS USED FOR 2 SEAS RUNS
tstp_nb = fake_seas .*2;
%tstp_nb = fake_seas .*2.*4;

%% FOR THE FREQ TESTING AND THE SS TESTING
%tstp_nb = season_long.*3;
% Testing the limits of the steady state
%tstp_nb = season_long.*6*10;
% Testing if steady state also reached while feeding sediment afterwards
%tstp_nb = season_long;
% Making a little test run to check what would happen in the flume
%tstp_nb = 3600 ./ dt .*50; % 50 stands for 50 hours of simulation
%tstp_nb = 3600 ./ dt .*100; % 100 stands for 50 hours of simulation

%tstp_nb = month_long*2 + 24*5;
%tstp_nb = 24*10;
t_end = tstp_nb* dt;
%t_end = 3 * (6*24)*dt; %days * (nb of time step in a day)

%Length of domain
Length = (M-1) * dx;
%Defining x and its staggered version for the solving
x = [0:dx:Length];
x_stag = (x(2:end) + x(1:end-1)) ./ 2;

%Defining the middle of each grid node for the staggered pdepe solver
mid_grid = x(1:end-1) + dx./2;
%Because I'll need that at some point
secondsinyear = 31556926;

%Defining the width of the glacier
W = 2000; % m

%Defining physical constants
rho_w = 1000;
rho_i = 910;
rho_s = 2650;
g = 9.81;
%Minimum channel size
%S_min = 1e-1;

% For the flume experiments
S_min = 1e-1;

A_tild = 5e-25;
n = 3;

lc = 2;
c_w = 4.22e3;
c_t = 7.5e-8;
L = 3.34e5;
mu_w = 1.787e-3; %Pa s / viscosity of water

r = (rho_s - rho_w)/rho_w;
Y = 5.0e10; % 5.0e4 MPa
k_v = 1.0e6; % [m^-1]
sigma_T = 7e6; % 7MPa
nu  = 10^(-6); %[m2/s] / inematic viscosity of water
kappa = 0.41; % Von Karman's constant
lambda_s  = 0.35; %porosity of bed sediment
A1 = 0.36;
tau_c = 0.03;

%D = 0.256; % m
%D = 0.256 *2/3; %m
%D = 0.256*2; % m
D = 0.06; % m
disp('D = 0.06 m')
%pause

q_tot = 22.64e-4.*4; %mass sed flux 
q_s     = q_tot * rho_s; % volumetric sediment flux
slope   = 0.0053;
theta   = slope;

%Manning roughness
n_ice = 0.01;
n_bed = 0.05;
%The friction coefficient is a function of the hydraulic radius
%here I only write down the top of the fraction
f_i_p1 = 8*g*(n_ice)^2;
f_b_p1 = 8*g*(n_bed)^2;
%Now averaged over channel walls
man_n = ((n_ice^2 * pi + 2*n_bed^2) ./ (pi + 2))^(1/2);
%Coefficient in the Werder version of writing the equations
%ADD maybe I can clean up kc from the cst list
kc = sqrt( 1 / (rho_w*g*man_n^2*(2/pi)^(2/3)*(pi+2)^(4/3) ) );
%Exponents for Manning equations
alpha_c = 5/4;
beta_c = 3/2;

%Ice temperature
T_i = 0;
%Thermal conductivity of water
K_w = 0.558; %  W * m^-1 * K^-1 / Value from Clarke 2003


%Ice surface shape
%IS runs:
%max_hi = 800; % m
%max_hi = 600; % m
%disp('600m geom')
max_hi = 700; % m REF GEOM

%To test the flume
%max_hi = 11;

%H_long = zeros(1,M+2) + max_hi.*sqrt(linspace(1,0,M+2));
% Making a glacier with some curvature that ends up with a 90m high terminus.
%H_long = zeros(1,M) + (max_hi-90).*(linspace(1,0,M)).^(1/1.3) + 90;

% Taking the terminus of 120 km long Leverett-like glacier with flat bed though...
%H_long = zeros(1,(M-1)*4+3) + (1450).*(linspace(1,0,(M-1)*4+3)).^(1/2);
%H(1:M) = H_long((M-1)*4-M+1:(M-1)*4+3-3);
%H = H + 90 - H(end);

% Making a glacier with some curvature that ends up with a 75m high terminus.
%H_long = zeros(1,M) + (max_hi-75).*(linspace(1,0,M)).^(1/1.3) + 75;

% For a thick-ish terminus
%H_long = linspace(max_hi,112,M);
% For a thin-ish terminus
% 75 m at the terminus seems to lead to way to much pile up... so going for 90
%H_long = linspace(max_hi,100,M); 
H_long = linspace(max_hi,90,M); 

%To test the flume:
%H_long = linspace(max_hi,10,M); 

H(1:M) = H_long(1:M);

%keyboard
%H(1:M) = H_long(M+1:M*2);

%staggered ice topo
H_st = (H(2:end) + H(1:end-1)) ./ 2;
%Ice surface slope
dHdx = (H(1:end-1) - H(2:end)) ./ dx;

%Bed elevation
%regular flat bed glacier
eta_br = H.*0;
%eta_br = eta_br + 1;    
%eta_br(end) = eta_br(end) + 2;
%eta_br(end-1) = eta_br(end-1) + 2*2/3;
%eta_br(end-2) = eta_br(end-2) + 2*1/3;
%eta_br(end-10:end) = eta_br(end-10:end) + 0.1;
%eta_br(1:10) = eta_br(1:10) + 1;

%% TO MAKE A SMALL WEDGE AND COMPARE HYDRAULICS WITH THE SEDIMENT WEDGE
%eta_br = eta_br + 0.017;
%eta_br(end) = 0.04226;
%eta_br(end-1) = 0.0199;
%eta_br(end-2) = 0.0198;
%eta_br(285) = 0.01869;
%eta_br(259) = 0.01761;

%Ice thickness
h_i = H - eta_br;

%Setting the thickness of sediment:
V_a_init = zeros(1,M);

if distributed_Vs_input == 1
    %V_a_init = V_a_init + 5*(1-lambda_s);
    %V_a_init = V_a_init + 1*(1-lambda_s);
    V_a_init = V_a_init + 0.5*(1-lambda_s);
    %V_a_init = V_a_init + 0.07*(1-lambda_s);
    %V_a_init = V_a_init + 0.05;
    %V_a_init(1:4) = [0 0 0 0];
    %V_a_init(1) = 100;
end

% Distributed source of sediment across the bed
distrib_source =  zeros(1,M);
%distrib_source = distrib_source + 3.6e-3; % Source imposed in ESurf paper...
if distributed_source == 1
    distrib_source = distrib_source + 3.6e-3./14;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Initial and boundary conditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Hydraulic potential at the ice surface
phi_0 = rho_w .*g .* eta_br + rho_i .* g .* h_i;
%init hyd pot in the channels (accounting for sediment thickness)
phi_0_c = rho_w.*g.*eta_br + rho_w*g*V_a_init + rho_i.*g.*(h_i-V_a_init);
%ice pressure
p_ice = rho_i .*g .* h_i;
%Hydraulic potential at the bed.
phi_br = rho_w .*g .* eta_br;
%Hydraulic potential at top of init sediment layer
phi_eta = rho_w.*g.*V_a_init;

%putting the hydraulic potential on a staggered grid to put into
%pdepe solver
phi_0_st = (phi_0(2:end) + phi_0(1:end-1)) ./ 2;
phi_br_st = (phi_br(2:end) + phi_br(1:end-1)) ./ 2;
phi_0_c_st = (phi_0_c(2:end) + phi_0_c(1:end-1)) ./ 2;

%initializing phi
phi_c_init = phi_0 .*0.5;
p_ice_init = phi_0_c - phi_br - phi_eta;
pc_init = phi_c_init - phi_br - phi_eta;
scale_pc = 1;%1e4;

if upstream_Qc_flux == 1
    %Flux of water at the upstream node
    % For regular SEAS4
    flux_bc = 50; % m^3/s?
    % SEAS4 but with less water
    %flux_bc = 40; % m^3/s?
    % To test the flume
    %flux_bc = 1;
    %flux_bc = 0.1;
elseif upstream_Qc_flux == 0
    flux_bc = 0; % m^3/s?
end

Q_fact = water_input_shape(3600./dt);
%Q_fact = 0;
%Q_fact = 1;

if upstream_qs_flux == 1
    %Flux of sediment at upstream node
    % That flux is q_tc(1) for SS 50 init run and D = 0.171
    %sed_flux = 0.01863;
    % Tryaing constant flux for real now
    %sed_flux = 0.016.*5.8;
    % Use a diameter of 7 for sims with minimum 50m^3/s
    %sed_flux = 0.016.*7;
    %sed_flux = 0.1120; %What came out of 6d oscillation simulations
    %sed_flux = 0.08; %Oscillation sims, a lot less comes out of the terminus when grid refined...
    % cst input flux for a cst Qch of 50m^3/s and WDG glacier
    %sed_flux = 0.14; % Now with that everything should be flushed out... so let's try
    %sed_flux = sed_flux .* 1.1; % Increase the sed input by small % to get things to pile up. 
    %sed_flux = 0.215;
    % Making up some sediment flux for my fake season input
    %sed_flux = 0.065; % REF FOR PAPERS RUNS FOR NOW
    %sed_flux = 0.075;
    sed_flux = 0.025; % for season with wings
    %sed_flux = 0.002; % for dx = 100m because more sensitive
    %disp('Sed flux with wings')
    % Testing the same as ESurf paper ref values
    %sed_flux = 9.1e-3;
elseif upstream_qs_flux == 0
    sed_flux = 0;
end

%Water input a each node of the domain
b_s_node = zeros(1,M);
b_c_node = zeros(1,M);

if distributed_bc_input == 1
    %b_c_node = b_c_node + 1e-6.*W;
    b_c_node = b_c_node + 1e-9.*W;
    %b_c_node(1:2) =[1 1].* 50./secondsinyear.*W;
end

if distributed_bs_input == 1
    b_s_node = b_c_node + 1e-9;
end


b_c = (b_c_node(1:end-1) + b_c_node(2:end)) ./ 2;
b_s = (b_s_node(1:end-1) + b_s_node(2:end)) ./ 2;


%Initializing a channel size
S_init = zeros(1,M);
% For all the esker simulations
S_init = S_init + 12;
% To test the flume:
%S_init = S_init + 1e-2;
S_init_stag = (S_init(2:end) + S_init(1:end-1)) ./ 2;

v_init = zeros(1,M)+1;
Tw_init = zeros(1,M);

R_H_init = S_init ./ ( (pi*sqrt((2.*S_init)./pi)) ./ (2*(pi+2)) );
f_R_init = 8*g*(man_n).^2 ./ (R_H_init.^(1/3));

%%%%%%%%%%%%%%%%%%%%
%GO FISH FOR SEASON DEFINITION LATER ON IF NECESSARY
%FOR NOW IT IS NOT NEEDED
%%%%%%%%%%%%%%%%%%%
%Compressibility of water
gamma = 1e-9;

% Bond on Fc so that solver actually calculates something
Fc_lim = 1e-9;

% Tolerance for ode15 solver
RelTol = 1.0e-06;%1.0e-06;       % default = 1.0e-03
AbsTol = 1.0e-08;%1.0e-08;       % default = 1.0e-06  

%initializing the vectors in which the quantities can then be stored
% length M (on the nodes)
pc = zeros(tstp_nb,M);
Sch = zeros(tstp_nb,M);
v = zeros(tstp_nb,M);
Tw = zeros(tstp_nb,M);
V_a = zeros(tstp_nb,M);
q_t = zeros(tstp_nb,M);
Vtr = zeros(tstp_nb,M);
store_q_t = zeros(tstp_nb,M);
store_s3 = zeros(tstp_nb,M);
store_dvadt = zeros(tstp_nb,M);
store_Fc = zeros(tstp_nb,M);
store_q_tt = zeros(tstp_nb,M);
% Trying ot make store_s3 more consistent?
%store_s3 = store_s3 + 1e-15;

% length M-1 (Staggered qtities)
store_q_s = zeros(tstp_nb,M-1);
store_q_tc = zeros(tstp_nb,M-1);
store_tau_s = zeros(tstp_nb,M-1);
store_v_w = zeros(tstp_nb,M-1);
store_qtc_eta = zeros(tstp_nb,M-1);
store_dphicdx = zeros(tstp_nb,M-1);
store_detadx = zeros(tstp_nb,M-1);
store_eta = zeros(tstp_nb,M-1);
dqtdx = zeros(tstp_nb,M-1);

%Now putting everything in structures
            %'mid_grid',mid_grid,...
cst = struct('M',M,...
            'rho_w',rho_w,...
            'rho_i',rho_i,...
            'rho_s',rho_s,...
            'g',g,...
            'S_min',S_min,...
            'A_tild',A_tild,... 
            'n',n,...
            'dx',dx,...
            'dt',dt,...
            't_end',t_end,...
            'tstp_nb',tstp_nb,...
            'x',x,...
            'length',Length,...
            'W',W,...
            'kc',kc,...
            'alpha_c',alpha_c,...
            'beta_c',beta_c,...
            'gamma',gamma,...
            'Fc_lim',Fc_lim,...
            'lc',lc,...
            'c_w',c_w,...
            'c_t',c_t,...
            'L',L,...
            'mu_w',mu_w,...
            'n_ice',n_ice,...
            'n_bed',n_bed,...
            'f_i_p1',f_i_p1,...
            'f_b_p1',f_b_p1,...
            'man_n',man_n,...
            'r',r,...
            'Y',Y,...
            'k_v',k_v,...
            'sigma_T',sigma_T,...
            'nu',nu,...
            'kappa',kappa,...
            'lambda_s',lambda_s,...
            'A1',A1,...
            'tau_c',tau_c,...
            'secondsinyear',secondsinyear,...
            'q_tot',q_tot,...
            'q_s',q_s,...
            'T_i',T_i,...
            'K_w',K_w,...
            'scale_pc',scale_pc,...
            'RelTol',RelTol,...
            'AbsTol',AbsTol);

sim_cond = struct('bed_feedback',bed_feedback,...
                    'load_init_run',load_init_run,...
                    'upstream_Qc_flux',upstream_Qc_flux,...
                    'distributed_bc_input',distributed_bc_input,...
                    'distributed_bs_input',distributed_bs_input,...
                    'upstream_qs_flux',upstream_qs_flux,...
                    'flux_per_uw',flux_per_uw,...
                    'downstream_qs_flux',downstream_qs_flux,...
                    'distributed_source',distributed_source,...
                    'distributed_Vs_input',distributed_Vs_input);
    
model_var = struct('phi_0',phi_0,...
            'phi_br',phi_br,...
            'p_ice',p_ice,...
            'H',H,...
            'eta_br',eta_br,...
            'h_i',h_i,...
            'D',D,...
            'b_s',b_s,...
            'b_c',b_c,...
            'flux_bc',flux_bc,...
            'Q_fact',Q_fact,...
            'sed_flux',sed_flux,...
            'distrib_source',distrib_source,...
            'S_init',S_init,...
            'S_init_stag',S_init_stag,...
            'p_ice_init',p_ice_init,...
            'pc_init',pc_init,...
            'V_a_init',V_a_init,...
            'phi_c_init',phi_c_init,...
            'f_R_init',f_R_init);

store_var = struct('pc',pc,...
                'Sch',Sch,...
                'V_a',V_a);
