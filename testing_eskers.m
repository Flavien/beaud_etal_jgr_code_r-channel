%Script to turn the esker ta mere solving script
%In here I initialize the simulations and extract what is solved for
%
%

function [pc_new,Sch_new,V_a_new] = testing_eskers(time_loop,model_var,cst,store_var,sim_cond)


    fprintf('%s %d %s %d \n','Clock still ticking? ',time_loop,' / ',cst.tstp_nb);

    tspan = [(time_loop-1).*cst.dt time_loop.*cst.dt];

    %Set the tolerance of the solver
    options = odeset('MaxStep',cst.dt,'RelTol',cst.RelTol,'AbsTol',cst.AbsTol,'NonNegative',1:3*cst.M);

    %initialize the variable as a function of you are in the simulation
    if time_loop == 1
        
        pc_sol = model_var.pc_init;
        S_ch = model_var.S_init;
        V_a = model_var.V_a_init ;
        V_a = V_a .* sqrt((2.*S_ch)./pi).*2;
        %And making sure some BC are satisfied...
        pc_sol(end) = 1000./cst.scale_pc;

        if sim_cond.upstream_qs_flux == 1
            %V_a(1) = model_var.sed_flux ./ cst.dx .* cst.dt .* 2.*sqrt((2.*S_ch(1))./pi);
        end

    else
        pc_sol = store_var.pc(time_loop-1,:)./cst.scale_pc;
        S_ch = store_var.Sch(time_loop-1,:);
        V_a = store_var.V_a(time_loop-1,:);

        %And making sure some BC are satisfied...
        pc_sol(end) = 1000./cst.scale_pc;

    end

    %RUN the ode15s solver to get the solution
    [T,Y] = ode15s(@test_fun,tspan,[S_ch pc_sol V_a],options,model_var,cst,sim_cond);
    
    % Extract the first solution component as u.
    Sch_new     = Y(end,1:cst.M)'.*cst.scale_pc;
    if model_var.flux_bc(1) > 2;
        Sch_new(1) = Sch_new(2);
    end 
    pc_new      = Y(end,1.*cst.M+1:2.*cst.M)';
    V_a_new       = Y(end,2.*cst.M+1:3.*cst.M)';

    % Cheating but making sure something is saved somewhere
    if time_loop == 7500
        save backup_at_7500.mat
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%   SUBFUNCTION WHERE EVERYTHING IS DEFINED
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dY_dt = test_fun(T,Y,model_var,cst,sim_cond)
    
%global store_q_t store_q_s store_q_tc store_tau_s store_v_w store_q_tt store_qtc_eta store_dphicdx time_loop
    %Extracting values out of the Y matrix
    S_ch    = Y(1:cst.M)'.*cst.scale_pc;
    pc_sol  = Y(1.*cst.M+1:2.*cst.M)';
    V_a  = Y(2.*cst.M+1:3.*cst.M)';

    V_a_stag = zeros(1,cst.M);
    V_a_stag(2:cst.M) = (V_a(2:end) + V_a(1:end-1)) ./ 2;
        V_slope = (V_a(2) - V_a(1));
        V_a_stag(1) = V_a_stag(2) - V_slope;
%V_a_stag(1) = V_a(1);

    %Now applying some boundary conditions I guess...
    pc_sol(cst.M) = 1000; %Pa Atmospheric BC at terminus
        %max_pc = cst.rho_w.*cst.g.*model_var.h_i;
        %pc_sol(pc_sol > max_pc) = max_pc(pc_sol > max_pc);

    %%%%%%%%%%%%%
    %Writing down the equations:

    %put a bound on channel preventing them to have a negative cross-sectional area
    S_test = find(S_ch < model_var.S_init(1));
    S_ch(S_test) = model_var.S_init(S_test);

    %Cross sections of flow in the bed an ice
    S_b = 0;
    S_i = S_ch - S_b;

    Sch_avg = (S_ch(2:end) + S_ch(1:end-1)) ./ 2;
    Si_avg = (S_i(2:end) + S_i(1:end-1)) ./ 2;

    %Radii and wetted perimeter
    [R_i,P_i,P_b,R_H,P_w] = radii_and_perimeters(S_i,Si_avg,Sch_avg,S_ch,cst);
    
    %Diameter of channel
    D_i = 2.* sqrt(2.*S_i./pi); 
    
    D_i_stag = zeros(1,cst.M);
    D_i_stag(2:cst.M) = (D_i(2:cst.M) + D_i(1:cst.M-1))./2;
    D_i_stag(1) = D_i(1);

    %Elevation of the bed
    %ADD the calculation of eta_a which will depend on the sedimentation history of the whole reach
    %if T < cst.dt * 144
    %    z_b = model_var.eta_br.*0;
    %else
        z_b = model_var.eta_br;
    %end

    % Computing the equivalent thickness of sediment right away
    eta_a = V_a ./ (1-cst.lambda_s)./ D_i;
    
    %Calculating some gradients, and averaged quantities
    if sim_cond.bed_feedback == 1
        phi_c = cst.rho_w.*cst.g.*(z_b + eta_a) + pc_sol;
    elseif sim_cond.bed_feedback == 0 
        phi_c = cst.rho_w.*cst.g.*z_b + pc_sol;
    end
        
    [dphicdx,dphicdx_avg,dpcdx,dpcdx_avg,pc_avg] = grad_phi_and_p(phi_c,pc_sol,cst);

    %Averaged friction coefficient
    [f_R,f_b] = friction_coeff(R_H,P_i,P_b,P_w,cst);

    %Changing the BC of Qch as function of time before calculating Qch
    [Qupstr] = Qch_BC(sim_cond,model_var,cst,T);
    % computing discharge
    [Q_c,Qc_avg] = discharge_comp(Sch_avg,P_w,f_R,dphicdx,model_var,cst,Qupstr);

    %Calculating and smoothing water velocity
    [v_w] = water_vel_smooth(Q_c,S_ch,Sch_avg);

    %Effective pressure
    if sim_cond.bed_feedback == 1
        N_ch = cst.rho_i .* cst.g .* (model_var.H - z_b - eta_a )./cst.scale_pc - pc_sol;
        %N_ch = cst.rho_i .* cst.g .* (model_var.H - z_b)./cst.scale_pc - pc_sol;
    elseif sim_cond.bed_feedback == 0
        N_ch = cst.rho_i .* cst.g .* (model_var.H - z_b)./cst.scale_pc - pc_sol;
    end

    Nch_avg = (N_ch(2:end) + N_ch(1:end-1)) ./ 2;

    %%%%%%%%%%%%%%%%%%%%%%%
    %   And now the sediment part

    %Calculating total and basal shear stresses
    [tau_tot,tau_b] = shear_stress(v_w,f_R,f_b,cst);

    %Shields stress
    tau_s = tau_b ./ ((cst.rho_s-cst.rho_w) .* cst.g .* model_var.D );

    tau_frac = tau_s ./ cst.tau_c;
    tau_frac(tau_frac < 1) = 1;

    %Transport capacity (Fernandez-Luque and van Beek, 1976)
    tau_diff = tau_s - cst.tau_c;
    tau_diff(tau_diff < 0) = 0;
    q_tc = 5.7 .* (cst.r .* cst.g .* model_var.D.^3).^(1/2) .* (tau_diff).^(3/2);


    % sediment source
    s3 = model_var.distrib_source;

    % CHANGE q_s_eta not actually used further, besides calculating the fraction covered
    % but the fraction covered is not even used anymore.
    q_s_eta = V_a .* cst.dx ./ cst.dt ./ sqrt((2.*S_i)./pi).*2 - s3.*cst.dx;

    if sim_cond.downstream_qs_flux == 1
        % NO FLUX BC AT OUTLET FOR SEDIMENT
        q_tc(end) = 0;
    end


    %Fraction of the bed covered
    [Fc,Fc_avg] = fraction_covered(q_s_eta,q_tc,cst);


    
    %Sediment velocity
    tau_frac = tau_s ./ cst.tau_c;
    tau_frac(tau_frac < 1) = 1;
    u_s = 1.56 .* (tau_frac -1).^0.56 .* sqrt(cst.r.*cst.g.*model_var.D);
    u_s(u_s > v_w) = v_w(u_s > v_w);

    %Testing again Inoue's method to see if it changes with different timesteps
    %V_bc = q_tc .* D_i ./ u_s;
    % STAG UPDATE
    q_tc_stag = zeros(1,cst.M);
    q_tc_stag(1:cst.M-1) = (q_tc(2:end) + q_tc(1:end-1)) ./ 2;

    q_tc_stag(cst.M) = q_tc_stag(cst.M-1) + q_tc(cst.M) - q_tc(cst.M-1);
    
    u_s_stag = zeros(1,cst.M);
    u_s_stag(1:cst.M-1) = (u_s(2:end) + u_s(1:end-1)) ./ 2;

    u_s_stag(cst.M) = u_s_stag(cst.M-1) + u_s(cst.M) - u_s(cst.M-1);

        %V_bc = q_tc .* D_i_stag ./ u_s;
    V_bc = q_tc_stag .* D_i ./ u_s_stag;
    V_ratio = V_a ./ V_bc;
        % STAG UPDATE
        %V_ratio = V_a_stag ./ V_bc;
        %V_ratio(q_tc <= 0) = 0;
    V_ratio(q_tc_stag <= 0) = 0;
    V_ratio(V_ratio >= 1) = 1;
        %q_b = V_ratio .* q_tc;
    q_b = V_ratio .* q_tc_stag;

    
    % STAG UPDATE
    q_tc_va = q_b.* D_i;
        %q_tc_va = q_b.* D_i_stag;

    % CHANGE
    % ADD At SOME POINT YOU WILL HAVE TO SOLVE FOR TOTAL VOLUME OF SEDIMENT IN THE CHANNEL
    % AND NOT FOR THE REACH AVERAGED VOLUME OF SED

    % ADD CHANGE
    % right now flux at upstream node a function of q_tc rather than given number!
    [dVa_dt] = Va_derivatives(q_tc_va,s3,cst,model_var,sim_cond,q_tc,D_i,D_i_stag);
    %if T>cst.dt*190
    %    keyboard;
    %end

    % FOR NOW BEDROCK EROSION IS NOT ACCOUNTED FOR
    v_e = zeros(1,cst.M-1);

    %%%%%%%%%%%%%%%%

    dQcdx = (Q_c(2:end)-Q_c(1:end-1))./cst.dx;
        % Trying to begin in a fake noe after terminus so that there is no discharge gradient
        %dQcdx(1:cst.M-1) = (Q_c(2:end)-Q_c(1:end-1))./cst.dx;

    %Changing the opening term...
    Aterm = Qc_avg./cst.L .* (dphicdx_avg -cst.c_t.*cst.c_w.*cst.rho_w.*dpcdx_avg);

        %TRying with that fake node at the end again...
        Xi = abs(Qc_avg.*dphicdx_avg);
        Pi = -cst.c_t.*cst.c_w.*cst.rho_w.*dpcdx_avg.*Qc_avg;
        v_oc_t = (Xi - Pi) ./ (cst.rho_i .* cst.L);

        v_cc_t = cst.A_tild .* S_i(1:cst.M-1) .* abs(N_ch(1:cst.M-1)).^(cst.n-1) .* N_ch(1:cst.M-1);
        % Capping the creep so that it can only close and not open a channel
        v_cc_t(v_cc_t < 0) = 0;

    %Starting with c
    c1_st = -cst.gamma .* S_ch(1:cst.M-1);


    if sim_cond.bed_feedback == 1
            v_s =  dVa_dt ./ (1-cst.lambda_s); 
            
    elseif sim_cond.bed_feedback == 0
        v_s = dVa_dt.*0;
    end

    %%%%%%%%%%%%%%%%%%
    % Writing down the time derivatives
    %keyboard
    dSch_dt(1:cst.M-1) = v_oc_t - v_cc_t - v_s(1:cst.M-1);
    dpch_dt(1:cst.M-1) = 1./c1_st .* (dSch_dt(1:cst.M-1) + dQcdx + Aterm./cst.rho_w - model_var.b_c);

    %Making sure the last node is 0 because not solved for
    dpch_dt(cst.M)    = 0;
    %Attempt at not making last node same as previous
    dSch_dt(cst.M)    = 2 * dSch_dt(cst.M-1) - dSch_dt(cst.M-2);
    
    % Giving a value to the temporal derivatives
    dY_dt = [dSch_dt dpch_dt./cst.scale_pc dVa_dt]';
    %if T>cst.dt.*24
    %    keyboard
    %end
